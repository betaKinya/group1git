package group1.tableEntities;

import java.io.Serializable;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQuery;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * The persistent class for the event_cause database table.
 * 
 */
@Entity
@XmlRootElement
@NamedQuery(name="EventCause.findAll", query="SELECT e FROM EventCause e")
public class EventCause implements Serializable, DataBaseEntity {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private EventCausePK id;

	private String description;

	public EventCause() {
	}

	public EventCausePK getId() {
		return this.id;
	}

	public void setId(EventCausePK id) {
		this.id = id;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}