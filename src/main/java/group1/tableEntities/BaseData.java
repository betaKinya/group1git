package group1.tableEntities;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * The persistent class for the base_data database table.
 * 
 */
@Entity
@XmlRootElement
@NamedQueries({
@NamedQuery(name="BaseData.findAll", query="SELECT b FROM BaseData b"),
@NamedQuery(name="BaseData.failuresForImsi", query="SELECT b FROM BaseData b where b.imsi = :imsi"),
@NamedQuery(name="BaseData.failuresForImsiTwo", query="SELECT b FROM BaseData b where b.imsi = :imsi AND b.dateTimeOfEvent Between :startDate and :finishDate"),
@NamedQuery(name="BaseData.failuresForImsibetween2dates", query="SELECT b FROM BaseData b where b.imsi = :imsi AND b.dateTimeOfEvent BETWEEN :startDate and :endDate"),//Count Call Failures
})
public class BaseData implements Serializable, DataBaseEntity {
	private static final long serialVersionUID = 1L;

	@Override
	public String toString() {
		return "BaseData [idBaseData=" + idBaseData + ", causeCode="
				+ causeCode + ", dateTimeOfEvent=" + dateTimeOfEvent
				+ ", duration=" + duration + ", eventId=" + eventId
				+ ", failureClass=" + failureClass + ", imsi=" + imsi
				+ ", networkElement=" + networkElement + ", ueType=" + ueType
				+ ", cellID=" + cellID + ", countryOperatorMccMnc="
				+ countryOperatorMccMnc + "]";
	}

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="idbasedata")
	private BigInteger idBaseData;

	@Column(name="cause_code")
	private int causeCode;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="date_time_of_event")
	private Date dateTimeOfEvent;

	private int duration;

	@Column(name="event_id")
	private int eventId;

	@Column(name="failure_class")
	private int failureClass;

	private String imsi;

	@Column(name="network_element")
	private String networkElement;

	@Column(name="ue_type")
	private int ueType;
	
	@Column(name="cell_id")
	private int cellID;

	//bi-directional many-to-one association to CountryOperatorMccMnc
	@ManyToOne
	@JoinColumns({
		@JoinColumn(name="market", referencedColumnName="market_mcc_id"),
		@JoinColumn(name="operator", referencedColumnName="operator_mnc_id")
		})
	private CountryOperatorMccMnc countryOperatorMccMnc;
	
	@Column(name="heir3_ID")
	private BigInteger heir3ID;
	
	@Column(name="heir32_ID")
	private BigInteger heir32ID;
	
	@Column(name="heir321_ID")
	private BigInteger heir321ID;

	public BaseData() {
	}

	public BigInteger getHeir3ID() {
		return heir3ID;
	}

	public void setHeir3ID(BigInteger heir3id) {
		heir3ID = heir3id;
	}

	public BigInteger getHeir32ID() {
		return heir32ID;
	}

	public void setHeir32ID(BigInteger heir32id) {
		heir32ID = heir32id;
	}

	public BigInteger getHeir321ID() {
		return heir321ID;
	}

	public void setHeir321ID(BigInteger heir321id) {
		heir321ID = heir321id;
	}

	public BigInteger getIdBaseData() {
		return idBaseData;
	}

	public void setIdBaseData(BigInteger idBaseData) {
		this.idBaseData = idBaseData;
	}

	public int getCauseCode() {
		return this.causeCode;
	}

	public void setCauseCode(int causeCode) {
		this.causeCode = causeCode;
	}

	public Date getDateTimeOfEvent() {
		return this.dateTimeOfEvent;
	}

	public void setDateTimeOfEvent(Date dateTimeOfEvent) {
		this.dateTimeOfEvent = dateTimeOfEvent;
	}

	public int getDuration() {
		return this.duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}

	public int getEventId() {
		return this.eventId;
	}

	public void setEventId(int eventId) {
		this.eventId = eventId;
	}

	public int getFailureClass() {
		return this.failureClass;
	}

	public void setFailureClass(int failureClass) {
		this.failureClass = failureClass;
	}

	public String getImsi() {
		return this.imsi;
	}

	public void setImsi(String imsi) {
		this.imsi = imsi;
	}

	public String getNetworkElement() {
		return this.networkElement;
	}

	public void setNetworkElement(String networkElement) {
		this.networkElement = networkElement;
	}

	public int getUeType() {
		return this.ueType;
	}

	public void setUeType(int ueType) {
		this.ueType = ueType;
	}


	public int getCellID() {
		return cellID;
	}

	public void setCellID(int cellID) {
		this.cellID = cellID;
	}

	public CountryOperatorMccMnc getCountryOperatorMccMnc() {
		return this.countryOperatorMccMnc;
	}

	public void setCountryOperatorMccMnc(CountryOperatorMccMnc countryOperatorMccMnc) {
		this.countryOperatorMccMnc = countryOperatorMccMnc;
	}

}