package group1.tableEntities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * The persistent class for the ue_table database table.
 * 
 */
@Entity
@XmlRootElement
@NamedQuery(name="UeTable.findAll", query="SELECT u FROM UeTable u")
public class UeTable implements Serializable, DataBaseEntity {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="tac_ue_type")
	private int tacUeType;

	@Column(name="access_capability")
	private String accessCapability;

	@Column(name="input_mode")
	private String inputMode;

	@Column(name="manufacturer_vendor")
	private String manufacturerVendor;

	@Column(name="marketing_name_model")
	private String marketingNameModel;

	private String os;

	@Column(name="ue_type")
	private String ueType;

	public UeTable() {
	}

	public int getTacUeType() {
		return this.tacUeType;
	}

	public void setTacUeType(int tacUeType) {
		this.tacUeType = tacUeType;
	}

	public String getAccessCapability() {
		return this.accessCapability;
	}

	public void setAccessCapability(String accessCapability) {
		this.accessCapability = accessCapability;
	}

	public String getInputMode() {
		return this.inputMode;
	}

	public void setInputMode(String inputMode) {
		this.inputMode = inputMode;
	}

	public String getManufacturerVendor() {
		return this.manufacturerVendor;
	}

	public void setManufacturerVendor(String manufacturerVendor) {
		this.manufacturerVendor = manufacturerVendor;
	}

	public String getMarketingNameModel() {
		return this.marketingNameModel;
	}

	public void setMarketingNameModel(String marketingNameModel) {
		this.marketingNameModel = marketingNameModel;
	}

	public String getOs() {
		return this.os;
	}

	public void setOs(String os) {
		this.os = os;
	}

	public String getUeType() {
		return this.ueType;
	}

	public void setUeType(String ueType) {
		this.ueType = ueType;
	}

}