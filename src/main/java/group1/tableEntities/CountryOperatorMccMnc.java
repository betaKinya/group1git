package group1.tableEntities;

import java.io.Serializable;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQuery;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * The persistent class for the country_operator_mcc_mnc database table.
 * 
 */
@Entity
@XmlRootElement
@NamedQuery(name="CountryOperatorMccMnc.findAll", query="SELECT c FROM CountryOperatorMccMnc c")
public class CountryOperatorMccMnc implements Serializable, DataBaseEntity {
	@Override
	public String toString() {
		return "CountryOperatorMccMnc [id=" + id + ", country=" + country
				+ ", operator=" + operator + "]";//Bottom comment taken out of this toString
	}//baseData=" + baseData + "]";

	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private CountryOperatorMccMncPK id;

	private String country;

	private String operator;

//	//bi-directional many-to-one association to BaseData
//	@OneToMany(mappedBy="countryOperatorMccMnc")
//	private List<BaseData> baseData;

	public CountryOperatorMccMnc() {
	}

	public CountryOperatorMccMncPK getId() {
		return this.id;
	}

	public void setId(CountryOperatorMccMncPK id) {
		this.id = id;
	}

	public String getCountry() {
		return this.country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getOperator() {
		return this.operator;
	}

	public void setOperator(String operator) {
		this.operator = operator;
	}

//	public List<BaseData> getBaseData() {
//		return this.baseData;
//	}
//
//	public void setBaseData(List<BaseData> baseData) {
//		this.baseData = baseData;
//	}
//
//	public BaseData addBaseData(BaseData baseData) {
//		getBaseData().add(baseData);
//		baseData.setCountryOperatorMccMnc(this);
//
//		return baseData;
//	}
//
//	public BaseData removeBaseData(BaseData baseData) {
//		getBaseData().remove(baseData);
//		baseData.setCountryOperatorMccMnc(null);
//
//		return baseData;
//	}

}