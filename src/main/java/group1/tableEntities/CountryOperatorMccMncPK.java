package group1.tableEntities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * The primary key class for the country_operator_mcc_mnc database table.
 * 
 */
@Embeddable
public class CountryOperatorMccMncPK implements Serializable, DataBaseEntity {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="market_mcc_id")
	private int marketMccId;

	@Column(name="operator_mnc_id")
	private int operatorMncId;

	public CountryOperatorMccMncPK() {
	}
	public int getMarketMccId() {
		return this.marketMccId;
	}
	public void setMarketMccId(int marketMccId) {
		this.marketMccId = marketMccId;
	}
	public int getOperatorMncId() {
		return this.operatorMncId;
	}
	public void setOperatorMncId(int operatorMncId) {
		this.operatorMncId = operatorMncId;
	}

	@Override
	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof CountryOperatorMccMncPK)) {
			return false;
		}
		CountryOperatorMccMncPK castOther = (CountryOperatorMccMncPK)other;
		return 
			(this.marketMccId == castOther.marketMccId)
			&& (this.operatorMncId == castOther.operatorMncId);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.marketMccId;
		hash = hash * prime + this.operatorMncId;
		
		return hash;
	}
}