package group1.tableEntities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * The primary key class for the event_cause database table.
 * 
 */
@Embeddable
public class EventCausePK implements Serializable, DataBaseEntity {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="cause_code")
	private int causeCode;

	@Column(name="event_id")
	private int eventId;

	public EventCausePK() {
	}
	public int getCauseCode() {
		return this.causeCode;
	}
	public void setCauseCode(int causeCode) {
		this.causeCode = causeCode;
	}
	public int getEventId() {
		return this.eventId;
	}
	public void setEventId(int eventId) {
		this.eventId = eventId;
	}

	@Override
	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof EventCausePK)) {
			return false;
		}
		EventCausePK castOther = (EventCausePK)other;
		return 
			(this.causeCode == castOther.causeCode)
			&& (this.eventId == castOther.eventId);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.causeCode;
		hash = hash * prime + this.eventId;
		
		return hash;
	}
}