package group1.tableEntities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * The persistent class for the failure_class database table.
 * 
 */
@Entity
@XmlRootElement
@NamedQueries({
@NamedQuery(name="FailureClass.findAll", query="SELECT f FROM FailureClass f"),
@NamedQuery(name="FailureClass.findFailure", query="SELECT f.description FROM FailureClass f where f.failureClass = :failureClass")
})
public class FailureClass implements Serializable, DataBaseEntity {
	private static final long serialVersionUID = 1L;

	@Override
	public String toString() {
		return "FailureClass [failureClass=" + failureClass + ", description="
				+ description + "]";
	}

	@Id
	@Column(name="failure_class")
	private int failureClass;

	private String description;

	public FailureClass() {
	}

	public int getFailureClass() {
		return this.failureClass;
	}

	public void setFailureClass(int failureClass) {
		this.failureClass = failureClass;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}