package group1.tableEntities;

import group1.entityEnumType.EntityType;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;

@Stateless
@LocalBean
public class DataBaseEntityFactory {

	public DataBaseEntity createEntity(EntityType entityType){
		DataBaseEntity dbEntity = null;
		if(entityType.toString().equalsIgnoreCase("BASEDATA")){
			dbEntity = new BaseData();
		}
		else if(entityType.toString().equalsIgnoreCase("EVENTCAUSE")){
			dbEntity = new EventCause();
		}
		else if(entityType.toString().equalsIgnoreCase("FAILURECLASS")){
			dbEntity = new FailureClass();
		}
		else if(entityType.toString().equalsIgnoreCase("COUNTRYOPERATOR")){
			dbEntity = new CountryOperatorMccMnc();
		}
		else if(entityType.toString().equalsIgnoreCase("USEREQUIPMENT")){
			dbEntity = new UeTable();
		}
		else if(entityType.toString().equalsIgnoreCase("USER")){
			dbEntity = new User();
		}
		return dbEntity;
	}
}
