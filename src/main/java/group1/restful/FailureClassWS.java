package group1.restful;

import group1.dao.FailureClassDAO;
import group1.tableEntities.FailureClass;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.Query;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

@Path("/failureclass")
@Stateless
@LocalBean
public class FailureClassWS {

	@EJB
	private FailureClassDAO failureClassDao;

	@GET
	@Path("/{failureClassId}")
	public FailureClass getCountFailuresForImsiBetweenTwoDates(
			@PathParam("failureClassId") int failureClassId){
		Query query = failureClassDao.getEntityManager().createNamedQuery(
				"FailureClass.findFailure");
		query.setParameter("failureClass", failureClassId);
		FailureClass singleFailure = (FailureClass) query.getSingleResult();
		return singleFailure;
	}
}
