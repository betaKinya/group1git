package group1.restful;

import group1.dao.BaseDataDAO;
import group1.tableEntities.BaseData;

import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.Query;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;


@Path("/base")
@Stateless
@LocalBean
public class BaseDataWS {

	@EJB
	private BaseDataDAO baseDataDao;
	
	String[] cols = {"dateTimeOfEvent", "countryOperatorMccMnc", "countryOperatorMccMnc",
			"causeCode", "eventId", "failureClass", "networkElement" , "imsi"};
	
	@GET
	@Path("datatable")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public BaseDataTable getData(
			@QueryParam("sEcho") Integer sEcho,
			@QueryParam("iDisplayLength") Integer iDisplayLength,
			@QueryParam("iDisplayStart") Integer iDisplayStart,
			@QueryParam("sSearch") String search,
			@QueryParam("iSortCol_0") String iSortCol_0,
			@QueryParam("sSearch") String sSearch,
			@QueryParam("sSortDir_0") String sSortDir_0) throws SQLException {
		
		Query countQuery = baseDataDao.getEntityManager().createQuery(
				"SELECT count(b.imsi) FROM BaseData b");
		final long totalRecords = (Long) countQuery.getSingleResult();
		System.out.println("getData"); System.out.println("invoking GET"); System.out.println("sEcho" + sEcho);
		System.out.println("idisplayLength" + iDisplayLength);
		System.out.println("idisplayStart" + iDisplayStart);
		System.out.println("sSearch" + search);
		System.out.println("sSortDir_0" + sSortDir_0);
		System.out.println("sSortCol_0" + iSortCol_0);
		System.out.println("sSearch" + sSearch);
		int col = Integer.parseInt(iSortCol_0);
		String colName = cols[col];
		final BaseDataTable baseDataTable = new BaseDataTable();
		Query query;
		query = baseDataDao.getEntityManager().createQuery(
				"SELECT b FROM BaseData b ORDER BY "
				+ colName + " " + sSortDir_0);
		if (!sSearch.equals("")) {
			String globeSearch = " where (networkElement like '%" + sSearch + "%'"
					+ " or imsi like '%" + sSearch + "%'" + " or b.countryOperatorMccMnc.country like '%" + sSearch + "%'" + " or b.countryOperatorMccMnc.operator like '%" + sSearch + "%')";
			query = baseDataDao.getEntityManager().createQuery("SELECT b FROM BaseData b " + globeSearch + " ORDER BY "
					+ colName + " " + sSortDir_0);
		}
		final List<BaseData> baseList = query.setMaxResults(iDisplayLength).setFirstResult(iDisplayStart).getResultList();
		baseDataTable.setAaData(baseList);
		baseDataTable.setiTotalDisplayRecords(String.valueOf(totalRecords));
		baseDataTable.setiTotalRecords(String.valueOf(totalRecords));
		baseDataTable.setsEcho(sEcho);
		return baseDataTable;
	}
	
	private int getCount(){
		Query query;
		query = baseDataDao.getEntityManager().createQuery("SELECT count(b.imsi) FROM BaseData b");
		int count = (Integer) query.getSingleResult();
		return count;
	}
	
	@GET
	@Path("datatable2/{startDate}/{endDate}")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public BaseDataTable getData2(
			@QueryParam("sEcho") Integer sEcho,
			@QueryParam("iDisplayLength") Integer iDisplayLength,
			@QueryParam("iDisplayStart") Integer iDisplayStart,
			@QueryParam("sSearch") String search,
			@QueryParam("iSortCol_0") String iSortCol_0,
			@QueryParam("sSearch") String sSearch,
			@QueryParam("sSortDir_0") String sSortDir_0,
			@PathParam("startDate") String startDate,
			@PathParam("endDate") String endDate
			) throws SQLException, ParseException {
		System.out.println(startDate);
		System.out.println(endDate);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date startDateStringToDate = sdf.parse(startDate);
		Date endDateStringToDate = sdf.parse(endDate);
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(endDateStringToDate);
		calendar.add(Calendar.DATE, 1);
		Date updatedEndDateStringToDate = calendar.getTime();
		Query countQuery = baseDataDao.getEntityManager().createQuery(
				"SELECT count(b.imsi) FROM BaseData b where b.dateTimeOfEvent >= :startDate AND b.dateTimeOfEvent <= :finishDate");
		countQuery.setParameter("startDate", startDateStringToDate);
		countQuery.setParameter("finishDate", updatedEndDateStringToDate);
		final long totalRecords = (Long) countQuery.getSingleResult();
		int col = Integer.parseInt(iSortCol_0);
		String colName = cols[col];
		final BaseDataTable baseDataTable = new BaseDataTable();
		Query query;
		query = baseDataDao.getEntityManager().createQuery("SELECT "
				+ "b FROM BaseData b where "
				+"b.dateTimeOfEvent BETWEEN :startDate AND :finishDate ORDER BY b." + colName +  " " + sSortDir_0);
		if (!sSearch.equals("")) {
			String globeSearch = " and (b.networkElement like '%" + sSearch + "%'"
					+ " or b.imsi like '%" + sSearch + "%'" + " or b.countryOperatorMccMnc.country like '%" + sSearch + "%'" + " or b.countryOperatorMccMnc.operator like '%" + sSearch + "%')";
			query = baseDataDao.getEntityManager().createQuery("SELECT "
					+ "b FROM BaseData b where "
					+"b.dateTimeOfEvent BETWEEN :startDate AND :finishDate" + globeSearch + " ORDER BY b." + colName +  " " + sSortDir_0);
		}
		query.setParameter("startDate", startDateStringToDate);
		query.setParameter("finishDate", updatedEndDateStringToDate);
		final List<BaseData> baseList = query.setMaxResults(iDisplayLength).setFirstResult(iDisplayStart).getResultList();
		baseDataTable.setAaData(baseList);
		baseDataTable.setiTotalDisplayRecords(String.valueOf(totalRecords));
		baseDataTable.setiTotalRecords(String.valueOf(totalRecords));
		baseDataTable.setsEcho(sEcho);
		return baseDataTable;
	}
	
	@GET
	@Path("datatable3/{startDate}/{endDate}")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public BaseDataTable getData3(
			@QueryParam("sEcho") Integer sEcho,
			@QueryParam("iDisplayLength") Integer iDisplayLength,
			@QueryParam("iDisplayStart") Integer iDisplayStart,
			@QueryParam("sSearch") String search,
			@QueryParam("iSortCol_0") String iSortCol_0,
			@QueryParam("sSearch") String sSearch,
			@QueryParam("sSortDir_0") String sSortDir_0,
			@PathParam("startDate") String startDate,
			@PathParam("endDate") String endDate
			) throws SQLException, ParseException {
		System.out.println(startDate);
		System.out.println(endDate);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date startDateStringToDate = sdf.parse(startDate);
		Date endDateStringToDate = sdf.parse(endDate);
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(endDateStringToDate);
		calendar.add(Calendar.DATE, 1);
		Date updatedEndDateStringToDate = calendar.getTime();
		Query countQuery = baseDataDao.getEntityManager().createQuery(
				"SELECT count(distinct b.imsi) FROM BaseData b where b.dateTimeOfEvent >= :startDate AND b.dateTimeOfEvent <= :finishDate");
		countQuery.setParameter("startDate", startDateStringToDate);
		countQuery.setParameter("finishDate", updatedEndDateStringToDate);
		final long totalRecords = (Long) countQuery.getSingleResult();
		int col = Integer.parseInt(iSortCol_0);
		String colName = cols[col];
		final BaseDataTable baseDataTable = new BaseDataTable();
		Query query;
		query = baseDataDao.getEntityManager().createQuery("SELECT "
				+ "b.imsi, count(*), sum(duration) FROM BaseData b where "
				+"b.dateTimeOfEvent BETWEEN :startDate AND :finishDate GROUP BY b.imsi ORDER BY b." + colName +  " " + sSortDir_0);
		if (!sSearch.equals("")) {
			String globeSearch = " and (b.networkElement like '%" + sSearch + "%'"
					+ " or b.imsi like '%" + sSearch + "%'" + " or b.countryOperatorMccMnc.country like '%" + sSearch + "%'" + " or b.countryOperatorMccMnc.operator like '%" + sSearch + "%')";
			query = baseDataDao.getEntityManager().createQuery("SELECT "
					+ "b.imsi, count(*), sum(duration) FROM BaseData b where "
					+"b.dateTimeOfEvent BETWEEN :startDate AND :finishDate" + globeSearch + " GROUP BY b.imsi ORDER BY b." + colName +  " " + sSortDir_0);
		}
		query.setParameter("startDate", startDateStringToDate);
		query.setParameter("finishDate", updatedEndDateStringToDate);
		final List<BaseData> baseList = query.setMaxResults(iDisplayLength).setFirstResult(iDisplayStart).getResultList();
		baseDataTable.setAaData(baseList);
		baseDataTable.setiTotalDisplayRecords(String.valueOf(totalRecords));
		baseDataTable.setiTotalRecords(String.valueOf(totalRecords));
		baseDataTable.setsEcho(sEcho);
		return baseDataTable;
	}
	
	
	@GET
	@Path("datatable4/{failureCode}")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public BaseDataTable getData4(
			@QueryParam("sEcho") Integer sEcho,
			@QueryParam("iDisplayLength") Integer iDisplayLength,
			@QueryParam("iDisplayStart") Integer iDisplayStart,
			@QueryParam("sSearch") String search,
			@QueryParam("iSortCol_0") String iSortCol_0,
			@QueryParam("sSearch") String sSearch,
			@QueryParam("sSortDir_0") String sSortDir_0,
			@PathParam("failureCode") int failureCode
			) throws SQLException, ParseException {
		System.out.println("FAILURE CODE");
		System.out.println(failureCode);
		Query countQuery = baseDataDao.getEntityManager().createQuery(
				"SELECT count(b.failureClass) FROM BaseData b where b.failureClass = :failureCode");
		countQuery.setParameter("failureCode", failureCode);
		final long totalRecords = (Long) countQuery.getSingleResult();
		System.out.println("TOTAL RECORDS");
		System.out.println(totalRecords);
		int col = Integer.parseInt(iSortCol_0);
		String colName = cols[col];
		final BaseDataTable baseDataTable = new BaseDataTable();
		Query query;
		query = baseDataDao.getEntityManager().createQuery("SELECT b.dateTimeOfEvent, b.imsi, b.failureClass FROM BaseData b "
				+ "where b.failureClass = :failureCode ORDER BY b." + colName +  " " + sSortDir_0);
		if (!sSearch.equals("")) {
			String globeSearch = " and (b.networkElement like '%" + sSearch + "%'"
					+ " or b.imsi like '%" + sSearch + "%'" + " or b.countryOperatorMccMnc.country like '%" + sSearch + "%'" + " or b.countryOperatorMccMnc.operator like '%" + sSearch + "%')";
			query = baseDataDao.getEntityManager().createQuery("SELECT b.dateTimeOfEvent, b.imsi, b.failureClass FROM BaseData b "
					+ "where b.failureClass = :failureCode" + globeSearch + " ORDER BY b." + colName +  " " + sSortDir_0);
		}
		query.setParameter("failureCode", failureCode);
		final List<BaseData> baseList = query.setMaxResults(iDisplayLength).setFirstResult(iDisplayStart).getResultList();
		baseDataTable.setAaData(baseList);
		baseDataTable.setiTotalDisplayRecords(String.valueOf(totalRecords));
		baseDataTable.setiTotalRecords(String.valueOf(totalRecords));
		baseDataTable.setsEcho(sEcho);
		return baseDataTable;
	}
	
	@GET
	@Path("datatable4")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public BaseDataTable getData5(
			@QueryParam("sEcho") Integer sEcho,
			@QueryParam("iDisplayLength") Integer iDisplayLength,
			@QueryParam("iDisplayStart") Integer iDisplayStart,
			@QueryParam("sSearch") String search,
			@QueryParam("iSortCol_0") String iSortCol_0,
			@QueryParam("sSearch") String sSearch,
			@QueryParam("sSortDir_0") String sSortDir_0
			
			) throws SQLException, ParseException {
	
		Query countQuery = baseDataDao.getEntityManager().createQuery(
				"SELECT count(b.failureClass) FROM BaseData b");
		final long totalRecords = (Long) countQuery.getSingleResult();
		int col = Integer.parseInt(iSortCol_0);
		String colName = cols[col];
		final BaseDataTable baseDataTable = new BaseDataTable();
		Query query;
		query = baseDataDao.getEntityManager().createQuery("SELECT b.dateTimeOfEvent, b.imsi, b.failureClass FROM BaseData b ORDER BY b." + colName +  " " + sSortDir_0);
		if (!sSearch.equals("")) {
			String globeSearch = " and (b.networkElement like '%" + sSearch + "%'"
					+ " or b.imsi like '%" + sSearch + "%'" + " or b.countryOperatorMccMnc.country like '%" + sSearch + "%'" + " or b.countryOperatorMccMnc.operator like '%" + sSearch + "%')";
			query = baseDataDao.getEntityManager().createQuery("SELECT b.dateTimeOfEvent, b.imsi, b.failureClass FROM BaseData b" + globeSearch + " ORDER BY b." + colName +  " " + sSortDir_0);
		}
		final List<BaseData> baseList = query.setMaxResults(iDisplayLength).setFirstResult(iDisplayStart).getResultList();
		baseDataTable.setAaData(baseList);
		baseDataTable.setiTotalDisplayRecords(String.valueOf(totalRecords));
		baseDataTable.setiTotalRecords(String.valueOf(totalRecords));
		baseDataTable.setsEcho(sEcho);
		return baseDataTable;
	}
	///////////////////////////////////////////////////////////////////////////
	
	@SuppressWarnings("unchecked")
	@GET
	@Path("/story14NOFailureClassSelectedForAffectedImsis")
	public List<BaseData> getFailureClassAffectedImsis(){
		Query query = baseDataDao.getEntityManager().createQuery(
		"SELECT b.dateTimeOfEvent, b.imsi, b.failureClass FROM BaseData b");
		List<BaseData> returnedBaseData = query.getResultList();
		return returnedBaseData;
	}
	
	
	@SuppressWarnings("unchecked")
	@GET
	@Path("/story14FailureClassSelectedAffectedImsis/{failureCode}")
	public List<BaseData> getCountFailuresForModelBetweenTwoDates(
			@PathParam("failureCode") int failureCode
			){
		Query query = baseDataDao.getEntityManager().createQuery(
				"SELECT b.dateTimeOfEvent, b.imsi, b.failureClass FROM BaseData b where b.failureClass = :failureCode");
		query.setParameter("failureCode", failureCode);
		List<BaseData> returnedBaseData = query.getResultList();
		return returnedBaseData;
	}
	
	
	@SuppressWarnings("unchecked")//Old Query
	@GET
	@Path("/story9countimsi/{startDate}/{endDate}")
	public List<BaseData> getImsiCountAndDurationBetweenTwoDates(
			@PathParam("startDate") String startDate,
			@PathParam("endDate") String endDate) throws ParseException, IllegalArgumentException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date startDateStringToDate = sdf.parse(startDate + " 00:00:00.0");
		Date endDateStringToDate = sdf.parse(endDate + " 00:00:00.0");
		Query query = baseDataDao.getEntityManager().createQuery("SELECT "
				+ "b.imsi, count(*), sum(duration) FROM BaseData b where b.dateTimeOfEvent BETWEEN :startDate AND :finishDate group by b.imsi");		
		query.setParameter("startDate", startDateStringToDate);
		query.setParameter("finishDate", endDateStringToDate);
		List<BaseData> listBaseData = query.getResultList(); 
		return listBaseData;
	}
	

	@GET
	@Path("search/{searchImsi}")
	public List<String> getAllImsiFailures(@PathParam("searchImsi") String searchImsi){
		Query query = baseDataDao.getEntityManager().createQuery(
				"SELECT DISTINCT(b.imsi) FROM BaseData b where b.imsi LIKE :code" );
		query.setParameter("code", "%" + searchImsi + "%");
		List<String> returnedBaseData = query.setMaxResults(10).getResultList();
		return returnedBaseData;
	}
	
	@GET
	@Path("search/phonemodel/{phone}")
	public List<Integer> getTacs(@PathParam("phone") int phone){
		Query query = baseDataDao.getEntityManager().createQuery(
				"SELECT DISTINCT(b.ueType) FROM BaseData b where b.ueType = :code" );
		query.setParameter("code",  phone);
		List<Integer> returnedBaseData = query.setMaxResults(10).getResultList();
		return returnedBaseData;
	}
	
	@GET
	@Path("story7allfailures")
	public List<BaseData> getAllImsiFailures(){
		Query query = baseDataDao.getEntityManager().createQuery(
				"SELECT b.dateTimeOfEvent, b.countryOperatorMccMnc, b.causeCode, "
				+ "b.eventId, b.failureClass, b.networkElement FROM BaseData b");
		List<BaseData> returnedBaseData = query.getResultList();
		return returnedBaseData;
	}
	
	@SuppressWarnings("unchecked")
	@GET
	@Path("/allfailures/{startDate}/{endDate}")
	public List<BaseData> getAllFailuresForImsiBetweenTwoDates(
			@PathParam("startDate") String startDate,
			@PathParam("endDate") String endDate) throws ParseException, IllegalArgumentException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date startDateStringToDate = sdf.parse(startDate + " 00:00:00.0");
		Date endDateStringToDate = sdf.parse(endDate + " 00:00:00.0");
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(endDateStringToDate);
		calendar.add(Calendar.DATE, 1);
		Date updatedEndDateStringToDate = calendar.getTime();
		Query query = baseDataDao.getEntityManager().createQuery("SELECT "
				+ "b.dateTimeOfEvent, b.countryOperatorMccMnc, b.causeCode, b.eventId, b.failureClass, b.networkElement, b.imsi  FROM BaseData b where "
				+"b.dateTimeOfEvent BETWEEN :startDate AND :finishDate group by b.imsi");		
		query.setParameter("startDate", startDateStringToDate);
		query.setParameter("finishDate", updatedEndDateStringToDate);
		List<BaseData> listBaseData = query.getResultList(); 
		return listBaseData;
	}
	
	// 344930000000011
	@GET
	@Path("imsifailuredropdown/{imsi}/{failureCode}")
	public List<BaseData> getBaseDatabyId(
			@PathParam("imsi") String imsi,
			@PathParam("failureCode") int failureCode){
		Query query = baseDataDao.getEntityManager().createQuery(
		"SELECT b.dateTimeOfEvent, b.causeCode, b.eventId FROM BaseData b where b.imsi = :imsi and b.failureClass = :failureCode");
		query.setParameter("imsi", imsi);
		query.setParameter("failureCode", failureCode);
		List<BaseData> returnedBaseData = query.getResultList();
		return returnedBaseData;
	}
	
	@SuppressWarnings("unchecked")
	@GET
	// 2013-01-12 17:15:00 //2013-01-10 17:15:00 //344930000000011
	@Path("/uniquecausecodes/{imsi}")
	public List<BaseData> uniqueCauseCodes(
			@PathParam("imsi") String imsi) {
		Query query = baseDataDao.getEntityManager().createQuery(
				"SELECT DISTINCT(b.causeCode)  FROM BaseData b where b.imsi = :imsi");
		query.setParameter("imsi", imsi);
		List<BaseData> baseDataList = query.getResultList();
		return baseDataList;
	}
	
	@SuppressWarnings("unchecked")
	@GET
	@Path("/failures/{startDate}/{endDate}/{imsi}")
	public List<BaseData> getCountFailuresForImsiBetweenTwoDates(
			@PathParam("startDate") String startDate,
			@PathParam("endDate") String endDate,
			@PathParam("imsi") String imsi) throws ParseException,
			IllegalArgumentException {
		
		List<BaseData> returnedBaseData = queryCountFailuresForImsiBetweenTwoDates(
				startDate, endDate, imsi);
		return returnedBaseData;
	}

	public List<BaseData> queryCountFailuresForImsiBetweenTwoDates(
			String startDate, String endDate, String imsi)
			throws ParseException {
		List<BaseData> returnedBaseData = new ArrayList<BaseData>();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date startDateStringToDate = sdf.parse(startDate);
		Date endDateStringToDate = sdf.parse(endDate);
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(endDateStringToDate);
		calendar.add(Calendar.DATE, 1);
		Date updatedEndDateStringToDate = calendar.getTime();
		Query query = baseDataDao.getEntityManager().createQuery("SELECT b FROM BaseData b where "
				+ "b.imsi = :imsi AND b.dateTimeOfEvent BETWEEN :startDate AND :finishDate");		
		query.setParameter("imsi", imsi);
		query.setParameter("startDate", startDateStringToDate);
		query.setParameter("finishDate", updatedEndDateStringToDate);
		returnedBaseData = query.getResultList();
		return returnedBaseData;
	}
	
	@SuppressWarnings("unchecked")
	@GET
	@Path("/story8failures/{startDate}/{endDate}/{ueType}")
	public List<BaseData> getCountFailuresForModelBetweenTwoDates(
			@PathParam("startDate") String startDate,
			@PathParam("endDate") String endDate,
			@PathParam("ueType") int ueType) throws ParseException,
			IllegalArgumentException {
		List<BaseData> returnedBaseData = queryCountFailuresForModelBetweenTwoDates(
				startDate, endDate, ueType);
		return returnedBaseData;
	}
	
	public List<BaseData> queryCountFailuresForModelBetweenTwoDates(
			String startDate, String endDate, int ueType)
			throws ParseException {
		List<BaseData> returnedBaseData = new ArrayList<BaseData>();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date startDateStringToDate = sdf.parse(startDate);
		Date endDateStringToDate = sdf.parse(endDate);
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(endDateStringToDate);
		calendar.add(Calendar.DATE, 1);
		Date updatedEndDateStringToDate = calendar.getTime();
		Query query = baseDataDao.getEntityManager().createQuery("SELECT b FROM BaseData b where "
				+ "b.ueType = :ueType AND b.dateTimeOfEvent BETWEEN :startDate AND :finishDate");		
		query.setParameter("ueType", ueType);
		query.setParameter("startDate", startDateStringToDate);
		query.setParameter("finishDate", updatedEndDateStringToDate);
		returnedBaseData = query.getResultList();
		return returnedBaseData;
	}

	// 344930000000011
	@GET
	@Path("/{id}")
	public List<BaseData> getBaseDatabyId(@PathParam("id") String id) {
		Query query = baseDataDao.getEntityManager().createQuery("SELECT b.dateTimeOfEvent, b.causeCode, b.eventId FROM BaseData b where "
				+ "b.imsi = :imsi");
		query.setParameter("imsi", id);
		List<BaseData> returnedBaseData = query.getResultList();
		return returnedBaseData;
	}
	
	
	@GET
	@Path("story10modelfailures/{ueType}")
	public List<BaseData> getModelFailures(
			@PathParam("ueType") int ueType){
		Query query = baseDataDao.getEntityManager().createQuery(
				"SELECT b.dateTimeOfEvent, b.eventId, b.causeCode, COUNT(*) FROM BaseData b WHERE b.ueType = :ueType GROUP BY b.eventId");
		query.setParameter("ueType", ueType);
		List<BaseData> returnedBaseData = query.getResultList();
		return returnedBaseData;
	}
	
	
	@GET
	@Path("story11toptencombos/{startDate}/{endDate}/")
	public List<BaseData> getTopTenCombos(
			@PathParam("startDate") String startDate,
			@PathParam("endDate") String endDate) throws ParseException,
			IllegalArgumentException {
		List<BaseData> returnedBaseData = topTenCombosBetweenTwoDates(
				startDate, endDate);
		return returnedBaseData;
	}
	
	
	public List<BaseData> topTenCombosBetweenTwoDates(
			String startDate, String endDate)
			throws ParseException {
		List<BaseData> returnedBaseData = new ArrayList<BaseData>();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date startDateStringToDate = sdf.parse(startDate);
		Date endDateStringToDate = sdf.parse(endDate);
		Query query = baseDataDao.getEntityManager().createQuery("SELECT b.countryOperatorMccMnc, b.cellID, COUNT(*) As Number_of_Failures FROM BaseData b WHERE "
				+ "(b.dateTimeOfEvent BETWEEN :startDate AND :finishDate) GROUP BY b.countryOperatorMccMnc.id.marketMccId, b.countryOperatorMccMnc.id.operatorMncId, b.cellID ORDER BY Number_of_Failures desc");		
		query.setMaxResults(10);
		query.setParameter("startDate", startDateStringToDate);
		query.setParameter("finishDate", endDateStringToDate);
		returnedBaseData = query.getResultList();
		return returnedBaseData;
	}
		
	
	@GET
	@Path("story12toptenimsifailures/{startDate}/{endDate}/")
	public List<BaseData> getTopTenImsiFailures(
			@PathParam("startDate") String startDate,
			@PathParam("endDate") String endDate) throws ParseException,
			IllegalArgumentException {
		List<BaseData> returnedBaseData = topImsiFailuresBetweenTwoDates(
				startDate, endDate);
		return returnedBaseData;
	}
	
	
	public List<BaseData> topImsiFailuresBetweenTwoDates(
			String startDate, String endDate)
			throws ParseException {
		List<BaseData> returnedBaseData = new ArrayList<BaseData>();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date startDateStringToDate = sdf.parse(startDate);
		Date endDateStringToDate = sdf.parse(endDate);
		Query query = baseDataDao.getEntityManager().createQuery("SELECT b.imsi, count(*) As Number_of_Failures FROM BaseData b WHERE (b.dateTimeOfEvent BETWEEN :startDate AND :finishDate)GROUP BY b.imsi ORDER BY Number_of_Failures desc");		
		query.setMaxResults(10);
		query.setParameter("startDate", startDateStringToDate);
		query.setParameter("finishDate", endDateStringToDate);
		returnedBaseData = query.getResultList();
		return returnedBaseData;
	}
		
	@POST
	public void addBaseData(BaseData baseData) {
		baseDataDao.addBaseData(baseData);
	}
	
	@GET
	@Path("story13toptenwithgraph/")
	public List<BaseData> getTopTenWithgraph() throws ParseException,
			IllegalArgumentException {
		List<BaseData> returnedBaseData = topTenWithGraph();
		return returnedBaseData;
	}
	
	
	public List<BaseData> topTenWithGraph()throws ParseException {
		List<BaseData> returnedBaseData = new ArrayList<BaseData>();
		Query query = baseDataDao.getEntityManager().createQuery("SELECT b.countryOperatorMccMnc, b.cellID, COUNT(*)"
				+ ", (SELECT COUNT(*) FROM BaseData b) FROM BaseData b "
				+ "GROUP BY b.countryOperatorMccMnc.id.marketMccId, b.countryOperatorMccMnc.id.operatorMncId, b.cellID");		
		query.setMaxResults(10);
		returnedBaseData = query.getResultList();
		return returnedBaseData;
	}
}
