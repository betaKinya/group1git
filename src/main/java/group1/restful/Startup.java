package group1.restful;

import group1.importData.ImportAllExcel;
import group1.validation.ErroneousCount;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

@Path("/init")
@RequestScoped
public class Startup {

	@EJB
	private ImportAllExcel reader;
	
	@POST
	@Path("/import")
	@Consumes("application/vnd.ms-excel")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public ErroneousCount startup(InputStream inputStream){
		File data = new File("import_data.xls");
		try {
			OutputStream outputStream = new FileOutputStream(data);
			
			byte[] buf = new byte[1024];
	        int bytesRead;
	        while ((bytesRead = inputStream.read(buf)) > 0) {
	            outputStream.write(buf, 0, bytesRead);
	        }
	        
	        outputStream.close();
	        inputStream.close();
		} catch (IOException e) {
			System.out.println("File Upload Failure 1: ");
		}
		reader.initialiseDatabase(data);
		int count = 0;
		String filePath="";
		try {
		File test = new File("C:\\Users\\Patrick\\Desktop\\HousePullDown\\erroneousrecords.xls");
		InputStream myxls = new FileInputStream(test);
		Workbook book = new HSSFWorkbook(myxls);
		Sheet sheet = book.getSheetAt(0);
		filePath= test.getAbsolutePath();
		count= sheet.getLastRowNum() +1;
		//System.out.println(sheet.getLastRowNum());
	} catch (FileNotFoundException e) {
		System.out.println("File Upload Failure 2: ");
		e.printStackTrace();
	} catch (IOException e) {
		System.out.println("File Upload Failure 3: ");
		e.printStackTrace();
	}
		ErroneousCount erroneousCount = new ErroneousCount(count, filePath);
		return erroneousCount;
	}
}
