package group1.restful;

import group1.tableEntities.BaseData;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class BaseDataTable {
	private Integer sEcho;
	private String iTotalRecords;
	private String iTotalDisplayRecords;
	private List<BaseData> aaData;
	
	public String getiTotalRecords() {
        return iTotalRecords;
    }
 
    public void setiTotalRecords(String iTotalRecords) {
        this.iTotalRecords = iTotalRecords;
    }
    
    public String getiTotalDisplayRecords() {
        return iTotalDisplayRecords;
    }
 
    public void setiTotalDisplayRecords(String iTotalDisplayRecords) {
        this.iTotalDisplayRecords = iTotalDisplayRecords;
    }
    
    public List<BaseData> getAaData() {
        return aaData;
    }
 
    public void setAaData(List<BaseData> aaData) {
        this.aaData = aaData;
    }
    public Integer getsEcho() {
        return sEcho;
    }
 
    public void setsEcho(Integer sEcho) {
        this.sEcho = sEcho;
    }
}