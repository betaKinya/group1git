package group1.validation;

import group1.dao.BaseDataDAO;
import group1.importData.WriteToDB;
import group1.tableEntities.BaseData;

import java.io.IOException;
import java.math.BigInteger;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;


@Stateless
@LocalBean
public class Validation {
	
	@EJB
	BaseDataDAO baseDataDAO;
	
	@EJB
	WriteToDB writeToDB;
	
	
	ErroneousRecords erroneousRecords= new ErroneousRecords();

	
	private HSSFWorkbook xlsbook;
	
	public Validation(){
		
	}
	
	public Validation(HSSFWorkbook xlsbook){
		this.xlsbook=xlsbook;
	}

	
	public List<Object> checkCellFormat(List<Object> rowList){
		Object cell0 =rowList.get(0);
		Object cell1 =rowList.get(1);
		if(!cell1.getClass().getSimpleName().equals("Double")){
			rowList.clear();
			return rowList;
		}
		Object cell2 =rowList.get(2);
		if(!cell2.getClass().getSimpleName().equals("Double")){
			rowList.clear();
			return rowList;
		}
		Object cell3 =rowList.get(3);
		if(!cell3.getClass().getSimpleName().equals("Double")){
			rowList.clear();
			return rowList;
		}
		Object cell4 =rowList.get(4);
		if(!cell4.getClass().getSimpleName().equals("Double")){
			rowList.clear();
			return rowList;
		}
		Object cell5 =rowList.get(5);
		if(!cell5.getClass().getSimpleName().equals("Double")){
			rowList.clear();
			return rowList;
		}
		Object cell6 =rowList.get(6);
		if(!cell6.getClass().getSimpleName().equals("Double")){
			rowList.clear();
			return rowList;
		}
		Object cell7 =rowList.get(7);
		if(!cell7.getClass().getSimpleName().equals("Double")){
			rowList.clear();
			return rowList;
		}
		Object cell8 =rowList.get(8);
		if(!cell8.getClass().getSimpleName().equals("Double")){
			rowList.clear();
			return rowList;
		}
		Object cell9 =rowList.get(9);
		if(!cell9.getClass().getSimpleName().equals("String")){
			rowList.clear();
			return rowList;
		}
		Object cell10 =rowList.get(10);
		if(!cell10.getClass().getSimpleName().equals("Double")){
			rowList.clear();
			return rowList;
		}
		Object cell11 =rowList.get(11);
		if(!cell11.getClass().getSimpleName().equals("Double")){
			rowList.clear();
			return rowList;
		}
		Object cell12 =rowList.get(12);
		if(!cell12.getClass().getSimpleName().equals("Double")){
			rowList.clear();
			return rowList;
		}
		Object cell13 =rowList.get(13);
		if(!cell13.getClass().getSimpleName().equals("Double")){
			rowList.clear();
			return rowList;
		}
		
		return rowList;
		
	}
	
	public void dateValidation(HSSFWorkbook xlsbook, BaseData data){
		this.xlsbook=xlsbook;
		
		
		boolean dateBool = false;
		Date date = data.getDateTimeOfEvent();
		
		if(date instanceof Date){
			dateBool = true;
		}
		
		if(dateBool){
			eventAndCauseCodeValidation(data);
		}else{
			try {
				erroneousRecords.addToExcel(data);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}	
		
	}
	
	public void eventAndCauseCodeValidation(BaseData data){
		boolean eventIdAndCauseCodeBool = false;
		int eventId = data.getEventId();
		int causeCode = data.getCauseCode();
		
		
		 HSSFSheet sheet = xlsbook.getSheetAt(1);
		 Iterator<Row> rowIterate = sheet.iterator();
		 rowIterate.next();
		 while(rowIterate.hasNext()){
			 Row currentRow = rowIterate.next();
			 int causeCodeCell = (int) Double.parseDouble(currentRow.getCell(0).toString());
			 int eventIdCell =(int) Double.parseDouble(currentRow.getCell(1).toString());
			 
			 if(eventIdCell==eventId && causeCodeCell==causeCode){
				 eventIdAndCauseCodeBool=true;
				 break;
			 }
		 }
		
		
		if(eventIdAndCauseCodeBool == true){
			failureClassValidation(data);
		}else{
			try {
				erroneousRecords.addToExcel(data);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}		
	}
	
	public void failureClassValidation(BaseData data){
		boolean failureClassBool = false;
		int failureClass = data.getFailureClass();
		
		
		 HSSFSheet sheet = xlsbook.getSheetAt(2);
		 Iterator<Row> rowIterate = sheet.iterator();
		 rowIterate.next();
		 while(rowIterate.hasNext()){
			 Row currentRow = rowIterate.next();
			 int currentCell =(int) Double.parseDouble(currentRow.getCell(0).toString());
			 
			 if(currentCell==failureClass){
				 failureClassBool=true;
				 break;
			 }
		 }
		
		if(failureClassBool){
			ueTypeValidation(data);
		}else{
			try {
				erroneousRecords.addToExcel(data);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}
	
	public void ueTypeValidation(BaseData data){
		
		int ueType = data.getUeType();
		boolean ueTypeBool = false;
		
		
		HSSFSheet sheet = xlsbook.getSheetAt(3);
		Iterator<Row> rowIterate = sheet.iterator();
		rowIterate.next();
		while(rowIterate.hasNext()){
			Row currentRow = rowIterate.next();
			 int currentCell =(int) Double.parseDouble(currentRow.getCell(0).toString());
			 
			 if(currentCell==ueType){
				 ueTypeBool=true;
			 }
		 }
		
		
		if(ueTypeBool){
			marketAndOperatorValidation(data);
		}else{
			try {
				erroneousRecords.addToExcel(data);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public void marketAndOperatorValidation(BaseData data){
		boolean marketAndOperatorBool = false;
		int market = data.getCountryOperatorMccMnc().getId().getMarketMccId();
		int operator = data.getCountryOperatorMccMnc().getId().getOperatorMncId();
		
		HSSFSheet sheet = xlsbook.getSheetAt(4);
		Iterator<Row> rowIterate = sheet.iterator();
		rowIterate.next();
		while(rowIterate.hasNext()){
			Row currentRow = rowIterate.next();
			 int marketCell =(int) Double.parseDouble(currentRow.getCell(0).toString());
			 int operatorCell = (int) Double.parseDouble(currentRow.getCell(1).toString());
			 
			 if(marketCell==market && operatorCell==operator){
				 marketAndOperatorBool=true;
			 }
		 }
		
		if(marketAndOperatorBool){
			cellIDValidation(data);
		}else{
			try {
				erroneousRecords.addToExcel(data);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	

	
	
	
	public void cellIDValidation(BaseData data){
		int cellID = data.getCellID();
		boolean cellIDBool = false;
		
		if(cellID>0){
			cellIDBool = true;
		}
		
		if(cellIDBool){
			durationValidation(data);
		}else{
			try {
				erroneousRecords.addToExcel(data);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}	
		
	}
	
	
	public void durationValidation(BaseData data){
		int duration = data.getDuration();
		boolean durationBool = false;
		
		if(duration>0){
			durationBool = true;
		}
		
		if(durationBool){
			neVersionValidation(data);
		}else{
			try {
				erroneousRecords.addToExcel(data);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}	
	}
	
	
	public void neVersionValidation(BaseData data){
		String neVersion=data.getNetworkElement();
		boolean neVersionBool = false;
		
		Pattern pattern = Pattern.compile("\\A\\d+[A|B]{1}\\z");
		Matcher matcher = pattern.matcher(neVersion);
		
		if(matcher.find()){
			neVersionBool = true;
		}
		
		if(neVersionBool){
			imsiValidation(data);
		}else{
			try {
				erroneousRecords.addToExcel(data);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}	
	}
	
	public void imsiValidation(BaseData data){
		//BigInteger imsiNumber = data.getImsi(); 
		String imsiString = data.getImsi();
		boolean imsiBool = false;
		//CHANGED AS WELL
//			Pattern pattern = Pattern.compile("\\d{15}");
//			Matcher matcher = pattern.matcher(imsiString);
//			
//			if(matcher.find()){
				imsiBool = true;
//			}
		
		
		
		if(imsiBool){
			cellHierValidation(data);
		}else{
			try {
				erroneousRecords.addToExcel(data);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}	
		
	}
	
	public void cellHierValidation(BaseData data){
		BigInteger cellHier3 = data.getHeir3ID();
		BigInteger cellHier32 = data.getHeir32ID();
		BigInteger cellHier321 = data.getHeir321ID();
		boolean cellHierBool = false;
		
		persistToDB(data);
		
	}
	
	public void persistToDB(BaseData data){
		baseDataDAO.addBaseData(data);
	}
}
