package group1.validation;

import group1.tableEntities.BaseData;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

public class ErroneousRecords {
	
	int counter = 0;
	int rownum = 0;

	public int getCounter() {
		return counter;
	}


	public void setCounter(int counter) {
		this.counter = counter;
	}


	public int getRownum() {
		return rownum;
	}


	public void setRownum(int rownum) {
		this.rownum = rownum;
	}


	HSSFWorkbook workbook = new HSSFWorkbook();
	HSSFSheet sheet = workbook.createSheet("Erroneous Records");
	
	
	public void addToExcel(BaseData data) throws IOException{


		//FileInputStream file = new FileInputStream(new File("C:\\Users\\Aoife\\Documents\\NewRepo2\\erroneousrecords"));


		//FileInputStream file = new FileInputStream(new File("C:\\Users\\Patrick\\Desktop\\HousePullDown\\erroneousrecords.xls"));
		
		int cellnum = 0;
		Row row = sheet.createRow(rownum);
	    
	    Cell cell1 = row.createCell(cellnum++);
	    cell1.setCellValue(HSSFDateUtil
				.getExcelDate(data.getDateTimeOfEvent()));
	   
	    
	    Cell cell2 = row.createCell(cellnum++);
	    cell2.setCellValue(String.valueOf(data.getEventId()));
	    

	    
	    Cell cell3 = row.createCell(cellnum++);
	    cell3.setCellValue(String.valueOf(data.getFailureClass()));
	    

	    
	    
	    Cell cell4 = row.createCell(cellnum++);
	    cell4.setCellValue(String.valueOf(data.getUeType()));
	    
	    Cell cell5 = row.createCell(cellnum++);
	    cell5.setCellValue(String.valueOf(data.getCountryOperatorMccMnc().getId().getMarketMccId()));
	    
	    Cell cell6 = row.createCell(cellnum++);
	    cell6.setCellValue(String.valueOf(data.getCountryOperatorMccMnc().getId().getOperatorMncId()));
	    
	    Cell cell7 = row.createCell(cellnum++);
	    cell7.setCellValue(String.valueOf(data.getCellID()));
	    
	    Cell cell8 = row.createCell(cellnum++);
	    cell8.setCellValue(String.valueOf(data.getDuration()));
	    
	    Cell cell9 = row.createCell(cellnum++);
	    cell9.setCellValue(String.valueOf(data.getCauseCode()));
	    
	    Cell cell10 = row.createCell(cellnum++);
	    cell10.setCellValue(data.getNetworkElement());
	    
	    Cell cell11 = row.createCell(cellnum++);
	    cell11.setCellValue((data.getImsi()).toString());
	    
	    Cell cell12 = row.createCell(cellnum++);
	    cell12.setCellValue((data.getHeir3ID()).toString());
	    
	    Cell cell13 = row.createCell(cellnum++);
	    cell13.setCellValue((data.getHeir32ID()).toString());
	    
	    Cell cell14 = row.createCell(cellnum++);
	    cell14.setCellValue((data.getHeir321ID()).toString());
	    
		 
		//file.close();


			//FileOutputStream out = new FileOutputStream(new File("C:\\Users\\Aoife\\Documents\\NewRepo2\\erroneousrecords"));


			//FileOutputStream out = new FileOutputStream(new File("C:\\Users\\Patrick\\Desktop\\HousePullDown\\erroneousrecords.xls"));

           //workbook.write(out);
           
           // out.close();	
		counter++;
		rownum++;
	}
}
