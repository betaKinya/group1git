package group1.validation;

public class ErroneousCount {
	@Override
	public String toString() {
		return "ErroneousCount [count=" + count + ", filePath=" + filePath
				+ "]";
	}
	private int count;
	private String filePath;
	public ErroneousCount(int count, String filePath) {
		super();
		this.count = count;
		this.filePath = filePath;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public String getFilePath() {
		return filePath;
	}
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}
	
	
}
