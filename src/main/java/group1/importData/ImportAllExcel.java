package group1.importData;

import group1.validation.ErroneousRecords;

import java.io.File;
import java.io.FileInputStream;
import java.util.Date;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Workbook;

@Stateless
@LocalBean
public class ImportAllExcel {
	@EJB
	ExcelRead excelRead;
	
	public void initialiseDatabase(File data){
		//System.out.print("in here!");
		//System.out.println(data.getName());
		try{//
			Date startDT = new Date();
			FileInputStream fis= new FileInputStream(data);
			HSSFWorkbook xlsbook= new HSSFWorkbook(fis);
			excelRead.readExcel(xlsbook, xlsbook.getSheetAt(4),4);
			excelRead.readExcel(xlsbook, xlsbook.getSheetAt(3),3);
			excelRead.readExcel(xlsbook, xlsbook.getSheetAt(2),2);
			excelRead.readExcel(xlsbook, xlsbook.getSheetAt(1),1);
			excelRead.readExcel(xlsbook, xlsbook.getSheetAt(0),0);
			System.out.println((new Date().getTime()-startDT.getTime())/1000+" seconds");
			fis.close();
			

		}catch(Exception e){
			e.printStackTrace();
		}
	}
}