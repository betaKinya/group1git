package group1.importData;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

@Stateless
@LocalBean
	public class ExcelRead {
	
	@EJB
	WriteToDB writeToDB;
	
	public void readExcel(HSSFWorkbook xlsbook, final HSSFSheet sheet, final int sheetIndex){
		
		List<Object> values = new ArrayList<Object>();
		Iterator<Row> rowIterate = sheet.iterator();
		rowIterate.next();//Move from top header row
		while(rowIterate.hasNext()){
			Row currentRow = rowIterate.next();
			for(int j =currentRow.getFirstCellNum(); j< currentRow.getLastCellNum(); j++ ){
				Cell currentCell = currentRow.getCell(j);
				switch(currentCell.getCellType()){
				case Cell.CELL_TYPE_NUMERIC:					
					values.add(currentCell.getNumericCellValue());
					break;
				case Cell.CELL_TYPE_STRING:
					values.add(currentCell.getStringCellValue());
					break;
				default:
					break;
				}
			}
			writeToDB.createAndStore(xlsbook, values, sheetIndex);
			//writeToDB.persistListsToDb();
			//dao.createAndStore(values, sheetIndex);	
			values.clear();
		}
	}
}
