package group1.importData;

import group1.dao.CountryOperatorMccMncDAO;
import group1.dao.DAOFactory;
import group1.dao.EventCauseDAO;
import group1.dao.FailureClassDAO;
import group1.dao.UeTableDAO;
import group1.entityEnumType.EntityType;
import group1.tableEntities.BaseData;
import group1.tableEntities.CountryOperatorMccMnc;
import group1.tableEntities.CountryOperatorMccMncPK;
import group1.tableEntities.DataBaseEntityFactory;
import group1.tableEntities.EventCause;
import group1.tableEntities.EventCausePK;
import group1.tableEntities.FailureClass;
import group1.tableEntities.UeTable;
import group1.validation.Validation;

import java.math.BigInteger;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

@Stateless
@LocalBean
public class WriteToDB {

	@EJB
	private DAOFactory daoFactory;
	
	@EJB
	private DataBaseEntityFactory dataBaseEntityFactory;

	@EJB
	private Validation validation;

	private int failureClassObject;
	private int causeCodeObject;
	
	public void createAndStore(HSSFWorkbook xlsbook, List<Object> rowList, final int sheetIndex) {
		
		switch (sheetIndex) {
			case 0:
				//BaseData data = new BaseData();
				BaseData data = (BaseData) dataBaseEntityFactory.createEntity(EntityType.BASEDATA);
				//DATE
				data.setDateTimeOfEvent(HSSFDateUtil.getJavaDate((Double) rowList.get(0)));
				// 1 and 2 EVENT ID AND CAUSECODE
				boolean causeEventCodeBool = false;
				int eventId = (int) Double.parseDouble(rowList.get(1)
						.toString());
				String causeCodeInput = rowList.get(8).toString();
				if (causeCodeInput.equals("(null)")) {
					causeCodeObject = 9999;
				} else {
					causeCodeObject = (int) (Double.parseDouble(causeCodeInput));
				}
				data.setEventId(eventId);
				data.setCauseCode(causeCodeObject);
				// 3 Failure Class
				String failureClassInput = rowList.get(2).toString();
				boolean failureClassBool = false;
				if (failureClassInput.contains("(null)")) {
					failureClassObject = 9999;
					failureClassInput = String.valueOf(9999);
				} else {
					failureClassObject = (int) (Double
							.parseDouble(failureClassInput));
				}
				data.setFailureClass(failureClassObject);
				// 4 UeType
				data.setUeType((int) Double.parseDouble(rowList.get(3).toString()));
				// 5 6 County and Operator
				boolean countryOperatorBool = false;
				CountryOperatorMccMncPK countryOperatorPK = new CountryOperatorMccMncPK();
				int marketMccID = (int) Double.parseDouble(rowList.get(4)
						.toString());
				countryOperatorPK.setMarketMccId(marketMccID);
				int operatorMncId = (int) Double.parseDouble(rowList.get(5)
						.toString());
				countryOperatorPK.setOperatorMncId(operatorMncId);
				CountryOperatorMccMnc countryOperator = (CountryOperatorMccMnc) dataBaseEntityFactory.createEntity(EntityType.COUNTRYOPERATOR);
				countryOperator.setId(countryOperatorPK);
				data.setCountryOperatorMccMnc(countryOperator);
				// 7 Cell ID
				data.setCellID((int) Double.parseDouble(rowList.get(6).toString()));
				data.setHeir3ID(BigInteger.valueOf((long) (Double.parseDouble(rowList.get(11).toString()))));
				data.setHeir32ID(BigInteger.valueOf((long) (Double.parseDouble(rowList.get(12).toString()))));
				data.setHeir321ID(BigInteger.valueOf((long) (Double.parseDouble(rowList.get(13).toString()))));
				// 8 Duration
				data.setDuration((int) Double.parseDouble(rowList.get(7).toString()));
				// 9 NE
				data.setNetworkElement(rowList.get(9).toString());
				// 10 IMSI
				data.setImsi(BigInteger.valueOf((long) (Double.parseDouble(rowList.get(10).toString()))).toString());
				validation.dateValidation(xlsbook, data);
				break;
			case 1:
				//EventCause eventCauseRow = new EventCause();
				EventCause eventCauseRow = (EventCause) dataBaseEntityFactory.createEntity(EntityType.EVENTCAUSE);
				EventCausePK id = new EventCausePK();
				id.setCauseCode((int) Double.parseDouble(rowList.get(0)
						.toString()));
				id.setEventId((int) Double.parseDouble(rowList.get(1)
						.toString()));
				eventCauseRow.setId(id);
				eventCauseRow.setDescription(rowList.get(2).toString());
				EventCauseDAO eventCauseDAO = (EventCauseDAO) daoFactory.createDAO(EntityType.EVENTCAUSE);
				eventCauseDAO.addEventCauseListData(eventCauseRow);
				break;
			case 2:
				FailureClass failureClassRow = (FailureClass) dataBaseEntityFactory.createEntity(EntityType.FAILURECLASS);
				failureClassRow.setFailureClass((int) Double
						.parseDouble(rowList.get(0).toString()));
				failureClassRow.setDescription(rowList.get(1).toString());
				FailureClassDAO failureClassDAO = (FailureClassDAO) daoFactory.createDAO(EntityType.FAILURECLASS);
				failureClassDAO.addFailureClassData(failureClassRow);
				break;
			case 3:
				UeTable ueTableRow = (UeTable) dataBaseEntityFactory.createEntity(EntityType.USEREQUIPMENT);
				ueTableRow.setTacUeType((int) Double.parseDouble(rowList.get(0)
						.toString()));
				ueTableRow.setMarketingNameModel(rowList.get(1).toString());
				ueTableRow.setManufacturerVendor(rowList.get(2).toString());
				ueTableRow.setAccessCapability(rowList.get(3).toString());
				ueTableRow.setUeType(rowList.get(6).toString());
				ueTableRow.setOs(rowList.get(7).toString());
				ueTableRow.setInputMode(rowList.get(8).toString());
				UeTableDAO ueTableDAO = (UeTableDAO) daoFactory.createDAO(EntityType.USEREQUIPMENT);
				ueTableDAO.addUeTableListData(ueTableRow);
				break;
			case 4:// CountryOperatorMccMnc Table
				//CountryOperatorMccMnc mccMncRow = new CountryOperatorMccMnc();
				CountryOperatorMccMnc mccMncRow = (CountryOperatorMccMnc) dataBaseEntityFactory.createEntity(EntityType.COUNTRYOPERATOR);
				CountryOperatorMccMncPK mccMncPk = new CountryOperatorMccMncPK();
				mccMncPk.setMarketMccId((int) Double.parseDouble(rowList.get(0)
						.toString()));
				mccMncPk.setOperatorMncId((int) Double.parseDouble(rowList.get(
						1).toString()));
				mccMncRow.setId(mccMncPk);
				mccMncRow.setCountry(rowList.get(2).toString());
				mccMncRow.setOperator(rowList.get(3).toString());
				CountryOperatorMccMncDAO countryOperatorMccMncDAO = (CountryOperatorMccMncDAO) daoFactory.createDAO(EntityType.COUNTRYOPERATOR);
				countryOperatorMccMncDAO.addCountryOperatorMccMnc(mccMncRow);
				break;
		}
	}

}
