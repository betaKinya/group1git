package group1.dao;

import group1.entityEnumType.EntityType;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

@Stateless
@LocalBean
public class DAOFactory {

	@EJB
	private BaseDataDAO baseDataDAO;
	@EJB
	private EventCauseDAO eventCauseDAO;
	@EJB
	private FailureClassDAO failureClassDAO;
	@EJB
	private CountryOperatorMccMncDAO countryOperatorDAO;
	@EJB
	private UeTableDAO ueTableDAO;

	public GenericDAO createDAO(EntityType entityType){
		GenericDAO dao = null;

		if(entityType.toString().equalsIgnoreCase("BASEDATA")){
			dao = baseDataDAO;
		}
		else if(entityType.toString().equalsIgnoreCase("EVENTCAUSE")){
			dao = eventCauseDAO;
		}
		else if(entityType.toString().equalsIgnoreCase("FAILURECLASS")){
			dao = failureClassDAO;
		}
		else if(entityType.toString().equalsIgnoreCase("COUNTRYOPERATOR")){
			dao = countryOperatorDAO;
		}
		else if(entityType.toString().equalsIgnoreCase("USEREQUIPMENT")){
			dao = ueTableDAO;
		}
		return dao;
	}
}
