package group1.dao;

import group1.tableEntities.UeTable;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
@LocalBean
public class UeTableDAO extends GenericJPADAO<UeTable, Long>{
	
	@PersistenceContext(unitName = "Tutorial")
	private EntityManager em;

    public UeTableDAO() {
		super(UeTable.class);
	}

	public UeTable getUeTableById(int id) {
        return em.find(UeTable.class, id);
    }
    
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void addUeTableListData(UeTable ueTable) {
            em.persist(ueTable);
    }
}