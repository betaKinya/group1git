package group1.dao;

import group1.tableEntities.FailureClass;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
@LocalBean
public class FailureClassDAO extends GenericJPADAO<FailureClass, Long> {

	@PersistenceContext(unitName = "Tutorial")
	private EntityManager em;

	public FailureClassDAO() {
		super(FailureClass.class);
	}

	public FailureClass getFailureClassId(int id) {
		return em.find(FailureClass.class, id);
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void addFailureClassData(FailureClass failureClass) {
		em.persist(failureClass);
	}

	@Override
	public void delete(FailureClass entity) {
		// TODO Auto-generated method stub
		
	}
}
