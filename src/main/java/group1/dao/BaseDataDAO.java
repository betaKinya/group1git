package group1.dao;

import group1.tableEntities.BaseData;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
@LocalBean
public class BaseDataDAO extends GenericJPADAO<BaseData, Long> {

	@PersistenceContext(unitName = "Tutorial")
	private EntityManager em;

	public BaseDataDAO() {
		 super(BaseData.class);
		// TODO Auto-generated constructor stub
	}

	public BaseData getBaseDataById(String id) {
		return em.find(BaseData.class, id);
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void addBaseData(BaseData baseData) {
		em.persist(baseData);
	}
		
	 @Override
	 public void delete(BaseData entity) {
	 // TODO Auto-generated method stub
	
	 }
}