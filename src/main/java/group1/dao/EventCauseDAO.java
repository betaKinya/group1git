package group1.dao;



import group1.tableEntities.EventCause;
import group1.tableEntities.EventCausePK;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
@LocalBean
public class EventCauseDAO extends GenericJPADAO<EventCause, Long>{
	
	@PersistenceContext(unitName = "Tutorial")
	private EntityManager em;

    public EventCauseDAO() {
	 	super(EventCause.class);
	}

	public EventCause getEventCauseById(int id) {
        return em.find(EventCause.class, id);
    }
    
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void addEventCauseListData(EventCause eventCause) {
            em.persist(eventCause);
    }

	public EventCause getEventCauseById(EventCausePK eventPk1) {
		return em.find(EventCause.class, eventPk1);
	}
}