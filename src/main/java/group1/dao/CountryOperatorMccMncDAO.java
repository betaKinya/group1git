package group1.dao;

import group1.tableEntities.CountryOperatorMccMnc;
import group1.tableEntities.CountryOperatorMccMncPK;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
@LocalBean
public class CountryOperatorMccMncDAO extends GenericJPADAO<CountryOperatorMccMnc, Long>{
	
	@PersistenceContext(unitName = "Tutorial")
	private EntityManager em;

	public CountryOperatorMccMncDAO() {
		super(CountryOperatorMccMnc.class);
	}
	
	public CountryOperatorMccMnc getCountryOperatorMccMncById(int id) {
        return em.find(CountryOperatorMccMnc.class, id);
    }
	
	public CountryOperatorMccMnc getCountryOperatorMccMncByPKID(CountryOperatorMccMncPK countryOperator ) {
        return em.find(CountryOperatorMccMnc.class, countryOperator);
    }
    
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void addCountryOperatorMccMnc(CountryOperatorMccMnc countryOperatorMccMnc) {
      em.persist(countryOperatorMccMnc);
    }
}
