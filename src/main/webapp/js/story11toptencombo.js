var rootURLStory11 = "";
	
var findAllStory11 = function() {
	myFunctionStory11();
	$.ajax({
		type : 'GET',
		url : rootURLStory11,
		dataType : "json",
		success : function(data){
			story11Top10ComboChart(data);
			document.getElementById("accordionStory11").style.visibility="visible";
			renderListStory11(data);
		}
	});
	
};

var renderListStory11 = function(data) {
	$("#table_bodystory11").empty();
	var list = data == null ? [] : (data instanceof Array ? data : [ data ]);
	$("#examplestory11").show();
	$.each(list, function(index, mov) {
		$('#table_bodystory11').append(
				'<tr><td>' + mov[0].id.marketMccId +'</td><td>'+ mov[0].id.operatorMncId +'</td><td>' 
				+ mov[1] + '</td><td>'+ mov[2]+'</td></tr>');
	});
	
	$("#examplestory11").dataTable({
		"order" : [[3, "desc"]]
	});
	$("#accordionStory11").accordion({heightStyle: "content"});
	//getter
	var collapsibleStory11 = $( "#accordionStory11" ).accordion( "option", "collapsible" );
	//setter
	$( "#accordionStory11" ).accordion( "option", "collapsible", true );
};

function myFunctionStory11() {
	console.log("ok2");
	var startdatestory11 = document.getElementById("startdatestory11").value;
	var enddatestory11 = document.getElementById("enddatestory11").value;
	rootURLStory11 += startdatestory11 + "/" + enddatestory11;	
}

$(document).ready(function(){
	$("#examplestory11").hide();
	$("#doSearchAgainStory11").click(function(){
		rootURLStory11 = "rest/base/story11toptencombos/";
		 findAllStory11();
		 return false;
	  });
});