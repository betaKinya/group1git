var rootURLStory12 = "";
	
var findAllStory12 = function() {
	myFunctionStory12();
	$.ajax({
		type : 'GET',
		url : rootURLStory12,
		dataType : "json",
		success : renderList12
	});
};

var renderList12 = function(data) {
	$("#table_bodystory12").empty();
	var list = data == null ? [] : (data instanceof Array ? data : [ data ]);
	$("#examplestory12").show();
	$.each(list, function(index, mov) {
		$('#table_bodystory12').append(
				'<tr><td>' + mov[0] +'</td><td>'+ mov[1] +'</td></tr>');
	});
	$('#examplestory12').dataTable({
		"order" : [[1, "desc"]]
	});
};

function myFunctionStory12() {
	var startdatestory12 = document.getElementById("startdatestory12").value;
	var enddatestory12 = document.getElementById("enddatestory12").value;
	rootURLStory12 += startdatestory12 + "/" + enddatestory12;	
}

$(document).ready(function(){
	$("#examplestory12").hide();
	$("#doSearchAgainStory12").click(function(){
		rootURLStory12 = "rest/base/story12toptenimsifailures/";
		 findAllStory12();
		 return false;
	  });
});