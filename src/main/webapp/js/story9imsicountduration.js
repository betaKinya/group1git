var oTable2;	

$(document).ready(function(){
	$("#examplelistimsicountduration").hide();	
	$("#doSearchlistimsicount2").click(function() {
		$("#examplelistimsicountduration").show();
		var startdate = document.getElementById("startdatestory9").value;
		var enddate = document.getElementById("enddatestory9").value;
		if(typeof oTable2 == "undefined"){
			oTable2 =  $('#examplelistimsicountduration').dataTable({
		    "aLengthMenu": [[5, 10, 20, -1], [5, 10, 20, "All"]],
		    "iDisplayLength": 5,
	        "bFilter": true,
	        "bRetrieve" : true,//Reinitialize datatable
	        "bLengthChange": false,
	        "sPaginationType": "full_numbers",
	        "aoColumns": [
	            { "mData": 0 },
	            { "mData": 1 },
	            { "mData": 2 }
	        ],
	        "bProcessing": true,
	        "bServerSide" :true,
	        "sAjaxSource": "rest/base/datatable3/" + startdate + "/" + enddate
	    });
		}else{
			console.log("ELSE");
			console.log(startdate2 + " " + enddate2);
			oTable2.fnClearTable();
			var oSettings = oTable2.fnSettings();
			oSettings.sAjaxSource  = "rest/base/datatable3/" + startdate + "/" + enddate;
			oTable2.fnDraw();
		}
	});
});