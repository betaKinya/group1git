var rootURLModel = "";
	
var findAll8 = function() {
	myFunction8();
	$.ajax({
		type : 'GET',
		url : rootURLModel,
		dataType : "json",
		success : renderList8
	});
};

var theDataModel;
var renderList8 = function(data) {
	theDataModel = data.length;
	$("#countparagraphModel").empty();
	$('#table_body_model').empty();
	var list = data == null ? [] : (data instanceof Array ? data : [ data ]);
	$("#exampleModel").show();
	$("#countparagraphModel").append("The Number of Call Failures for this model: " + theDataModel);
	$.each(list, function(index, mov) {
		var date = new Date(mov.dateTimeOfEvent).toLocaleDateString();//Date with GMT and Time
		$('#table_body_model').append(
				'<tr><td>' + date + '</td><td>' + mov.countryOperatorMccMnc.country +'</td><td>' + mov.countryOperatorMccMnc.operator +'</td><td>' 
				+ mov.causeCode +'</td><td>' + mov.eventId +'</td><td>' + mov.failureClass +'</td><td>' 
				+ mov.networkElement + '</td></tr>');
	});
	$('#exampleModel').dataTable({
        "aLengthMenu": [[5, 10, 20, -1], [5, 10, 20, "All"]],
        "iDisplayLength": 5
    });
	
};

function myFunction8() {
	console.log("ok2");
	var startdate = document.getElementById("startdate3").value;
	var enddate = document.getElementById("enddate3").value;
	var model = document.getElementById("model").value;
	rootURLModel += startdate + "/" + enddate + "/" + model;	
}

function findByModelAgain(searchModel) {
	console.log('findByImsi: ' + searchModel);
	$.ajax({
		type: 'GET',
		url: "rest/base/search/phonemodel/" + searchModel,
		dataType: "json",
		success: renderListModelAgain 
	});
}

var renderListModelAgain = function(data) {
	 $( "#model" ).autocomplete({
	      source: data
	 });
};
var searchString10 = "";
$(document).ready(function(){
	$("#exampleModel").hide();
	$("#doSearchAgainModel").click(function(){
		rootURLModel = "rest/base/story8failures/";
		 findAll8();
		 return false;
	  });
	$('#model').on('keydown', function(e){//NEED KEYDOWN FOR BACKSPACE
		if(e.which == 8){
			searchString10 = searchString10.slice(0, -1);
		}
	});
	$('#model').on('keypress', function(e){
		var imsiInput = $('#model').val();
			if(searchString10.length == 0){
				searchString10 = imsiInput;
			}
			else{
				var lastChar = imsiInput.slice(-1);
				searchString10 = searchString10 + lastChar;
			}	
			findByModelAgain($('#model').val());
			console.log("search");
			console.log(searchString10);
	});
	
});