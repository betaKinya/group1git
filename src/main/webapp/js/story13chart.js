function story13Top10ComboChart(data) {
	var list = data == null ? [] : (data instanceof Array ? data : [ data ]);
	var primaryColumnArray = [];
	$.each(list, function(index, listRow) {
		var primaryColumn = listRow[0].country;
		if (primaryColumnArray.indexOf(primaryColumn) == -1) {
			primaryColumnArray.push(primaryColumn);
		}
	});

	var highLevelData = [];
	var lowLevelData = [];

	$.each(primaryColumnArray, function(index, arrayRow) {
		var primaryColumn = arrayRow;
		var totalCount = 0;
		var seriesObject = [];
		$.each(list, function(index, listRow) {
			if (listRow[0].country == arrayRow) {
				totalCount += listRow[2];
				seriesObject.push([listRow[1].toString(),
						listRow[2]]);
			}
		});
		var tempObjectWithAttr = {
			name : primaryColumn,
			y : totalCount,
			drilldown : primaryColumn
		};
		highLevelData.push(tempObjectWithAttr);
		lowLevelData.push({
			id : primaryColumn,
			data : seriesObject
		});
	});

	$('#story13GraphResize').highcharts({
		chart : {type : 'bar'},
		yAxis : {
			tickPixelInterval : 50,
			title : {text : 'Count'}
		},
		title : {text : ''},
		xAxis : {
			type : 'category',
			title : {text : 'Country/Cell ID'}
		},
		legend : {enabled : true},
		plotOptions : {
			series : {
				borderWidth : 0,
				dataLabels : {
					enabled : true,
				}
			}
		},
		series : [ {
			name : 'Country Failure Count',
			colorByPoint : true,
			data : highLevelData
		} ],
		drilldown : {series : lowLevelData}
	});
};
