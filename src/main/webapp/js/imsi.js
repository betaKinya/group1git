var rootURLImsi = "";

var renderList2 = function(data) {
	$("#table_body").empty();
	$("#exampleimsi").show();
	var list = data == null ? [] : (data instanceof Array ? data : [ data ]);
	$.each(list, function(index, mov) {
		var date = new Date(mov[0]).toLocaleDateString();//Date with GMT and Time
		$('#table_body').append(
				'<tr><td>' + date + '</td><td>' + mov[1]
				+'</td><td>' + mov[2]
						+ '</td></tr>');
	});
	$('#exampleimsi').dataTable();
};

var findAllImsi = function() {
	myFunctionImsi();
	console.log('findAll');
	$.ajax({
		type : 'GET',
		url : rootURLImsi,
		dataType : "json",
		success : renderList2
	});
};

function myFunctionImsi() {
	var failureDropNumeralValue = 0;
	var imsi = document.getElementById("imsivalue").value;
	var failuredrop = document.getElementById("failuredrop").value;
	if(failuredrop == "DEFAULT"){
	rootURLImsi = "rest/base/" + imsi;
	}
	else{
		if(failuredrop == "EMERGENCY"){
			failureDropNumeralValue = 0;
		}
		else if(failuredrop == "HIGHPRIORITYACCESS"){
			failureDropNumeralValue = 1;
		}
		else if(failuredrop == "MTACCESS"){
			failureDropNumeralValue = 2;
		}
		else if(failuredrop == "MOSIGNALLING"){
			failureDropNumeralValue = 3;
		}
		else if(failuredrop == "MODATA"){
			failureDropNumeralValue = 4;
		}
		rootURLImsi = "rest/base/imsifailuredropdown/" + imsi + "/" + failureDropNumeralValue;
	}
}

$(document).ready(function() {
	$("#exampleimsi").hide();
	
	$("#doSearch").click(function() {
		findAllImsi();
		return false;
	});
	$("#logoutbutton").click(function() {
		sessionStorage.setItem('usertype', "");
		window.location.href = "index.html";// Opens HTML Page
	});
	$("#backbutton").click(function() {
		window.location.href = "commons.html";// Opens HTML Page
	});
});