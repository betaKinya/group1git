var rootURLImport = "rest/";
// Variable to store the file
var file;
var count = 0;
var fileName;
// Add events
$('input[type=file]').on('change', prepareFile);

// Grab the files and set them to the variable
function prepareFile(event) {
	    fileName = this.value;
	    var fileExtension = fileName.substr(fileName.length - 4);

	    console.log(fileExtension);
	    if (fileExtension != ".xls") {
	    	$('#wrongFileTypeError').dialog("open");
	        $("#import").hide();
	    }else{
	    	 $("#import").show();
	    	file = event.target.files[0];
	    }
};

var renderListImport = function(data) {
var list = data == null ? [] : (data instanceof Array ? data : [ data ]);
	document.getElementById("progressBar").style.visibility="hidden";

	$("#innerBody").children(':not(#progressBar)').fadeTo("slow",1, function() {
		  // Animation complete.
		});
$.each(list, function(index, erroneousCount) {
	$('#importCompleteDialog').append('<p>All data has now been imported</p><p>'
			+erroneousCount.count+' records have been moved to an Excel file</p>');
	});
	$('#importCompleteDialog').dialog("open");

};

var uploadFile = function() {
	var fileCheck = $("#datafile")[0];
	if(count>0){
		$('#fileAlreadyImportedError').dialog("open");
		return;
	}
	if (fileCheck.files === undefined || fileCheck.files.length == 0) {
		$('#noFileChosenError').dialog("open");
		return;
	} 
//	document.getElementById("importText").innerHTML = "Importing "+file.name+" to the database. Please don't close browser.";
//	
	

	

	$("#innerBody").children(':not(#progressBar)').fadeTo("fast", 0.3, function() {
//		   Animation complete.
	});

	

	

	


	
	document.getElementById("progressBar").style.visibility="visible";
	
	
	
	$.ajax({
		type : 'POST',
		data : file,
		url : rootURLImport + "init/import?filename=" + file.name + "&mimeType="
				+ file.type,
		cache : false,
		processData : false,
		contentType : file.type,
		success : renderListImport
	});
	count++;
};

$(document).ready(function() {

	$("#noFileChosenError").dialog({
		autoOpen: false,
	      show: {
	        effect: "blind",
	        duration: 1000
	      },
	      hide: {
	        effect: "explode",
	        duration: 1000
	      }
	});
	$("#wrongFileTypeError").dialog({
		autoOpen: false,
		width : 350,
	      show: {
	        effect: "blind",
	        duration: 1000
	      },
	      hide: {
	        effect: "explode",
	        duration: 1000
	      }
	});
	$("#importCompleteDialog").dialog({
		autoOpen: false,
		width : 500,
	      show: {
	        effect: "blind",
	        duration: 1000
	      },
	      hide: {
	        effect: "explode",
	        duration: 1000
	      }
	});
	$("#fileAlreadyImportedError").dialog({
		autoOpen: false,
		width : 500,
	      show: {
	        effect: "blind",
	        duration: 1000
	      },
	      hide: {
	        effect: "explode",
	        duration: 1000
	      }
	});
	$('#import').on('click', uploadFile);
	$("#logoutbutton").click(function(){
		sessionStorage.setItem('usertype', "");
		window.location.href = "index.html";//Opens HTML Page
	  });
	$("#backbutton").click(function(){
		window.location.href = "commons.html";//Opens HTML Page
	  });
	
});