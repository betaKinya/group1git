var rootURLlistimsiengineercallfailures = "";
var oTable;
var oTableAll;

function myFunctionlistimsiCF() {
	var startdate2 = document.getElementById("startdate2").value;
	var enddate2 = document.getElementById("enddate2").value;
	$("#examplelistimsiengineercallfailures").show();
	if(typeof oTable == "undefined"){
	console.log("IF");
    oTable = $('#examplelistimsiengineercallfailures').dataTable({
    	"aLengthMenu": [[5, 10, 20, -1], [5, 10, 20, "All"]],
        "iDisplayLength": 5,
        "bFilter": true,
        "bRetrieve" : true,
        "bLengthChange": false,
        "bDestroy": true,
        "sPaginationType": "full_numbers",
        "aoColumns": [
            { "mData": "more","mRender" : function (data,type, full){
	                	return new Date(full.dateTimeOfEvent).toLocaleDateString();
	            }},
	            { "mData": "countryOperatorMccMnc.country" },
	            { "mData": "countryOperatorMccMnc.operator" },
	            { "mData": "causeCode" },
	            { "mData": "eventId" },
	            { "mData": "failureClass" },
	            { "mData": "networkElement" },
	            { "mData": "imsi"}
        ],
        "bProcessing": true,
        "bServerSide" :true,
        "sAjaxSource": "rest/base/datatable2/" + startdate2 + "/" + enddate2
    });
	}else{
		console.log("ELSE");
		console.log(startdate2 + " " + enddate2);
		oTable.fnClearTable();
		var oSettings = oTable.fnSettings();
		 oSettings.sAjaxSource  = "rest/base/datatable2/" + startdate2 + "/" + enddate2;
		oTable.fnDraw();
	}
}

$(document).ready(function(){
	$("#examplelistimsiengineercallfailures").hide();
	$("#retrieveallimsidetailsbutton").click(function() {
		console.log("FINDALLDATATABLE");
		$("#examplelistimsiengineercallfailures").show();
		if(typeof oTableAll == "undefined"){
			if(typeof oTable != "undefined"){
				oTable.fnClearTable();
				console.log("CLEAR TABLE");
			}
			console.log("ABOUT TO MAKE oTableAll");
			oTableAll =  $('#examplelistimsiengineercallfailures').dataTable({
			"aLengthMenu": [[5, 10, 20, -1], [5, 10, 20, "All"]],
		    "iDisplayLength": 5,
	        "bFilter": true,
	        "bRetrieve" : true,//Reinitialize datatable
	        "bLengthChange": false,
	        "sPaginationType": "full_numbers",
	        "aoColumns": [
	            { "mData": "more","mRender" : function (data,type, full){
	                	return new Date(full.dateTimeOfEvent).toLocaleDateString();
	            }},
	            { "mData": "countryOperatorMccMnc.country" },
	            { "mData": "countryOperatorMccMnc.operator" },
	            { "mData": "causeCode" },
	            { "mData": "eventId" },
	            { "mData": "failureClass" },
	            { "mData": "networkElement" },
	            { "mData": "imsi"}
	        ],
	        "bProcessing": true,
	        "bServerSide" :true,
	        "sAjaxSource": "rest/base/datatable"
	    });
		}else{
			console.log("ELSE");
			console.log(startdate + " " + enddate);
			oTableAll.fnClearTable();
			var oSettings = oTableAll.fnSettings();
			oSettings.sAjaxSource  = "rest/base/datatable";
			oTableAll.fnDraw();
		}
	});
	
	$("#doSearchlistimsiengineercallfailures").click(function(){
		rootURLlistimsiengineercallfailures = "rest/base/allfailures/";
		 myFunctionlistimsiCF();
		 //findAll();
		 return false;
	  });
});