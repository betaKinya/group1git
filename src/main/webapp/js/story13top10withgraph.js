var rootURLStory13 = "";

var findAllStory13 = function() {
	$.ajax({
		type : 'GET',
		url : rootURLStory13,
		dataType : "json",
		success : function(data) {
			story13Top10ComboChart(data);
			renderListStory13(data);
		}
	});
};

var renderListStory13 = function(data) {
	$("#table_bodystory13").empty();
	var list = data == null ? [] : (data instanceof Array ? data : [ data ]);
	$("#examplestory13").show();
	$.each(list, function(index, mov) {
		$('#table_bodystory13').append(
				'<tr><td>' + mov[0].id.marketMccId + '</td><td>'
						+ mov[0].id.operatorMncId + '</td><td>' + mov[1]
						+ '</td><td>' + mov[2] + '</td><td>'
						+ ((mov[2] / mov[3]) * 100).toFixed(5) + '</td></tr>');
		console.log(mov[3]);
	});
	$('#examplestory13').dataTable({
		"order" : [[3, "desc"]]
	});
	$("#accordionStory13").accordion({heightStyle: "content"});
	//getter
	var collapsible = $( "#accordionStory13" ).accordion( "option", "collapsible" );
	//setter
	$( "#accordionStory13" ).accordion( "option", "collapsible", true );
};

$(document).ready(function() {
	rootURLStory13 = "rest/base/story13toptenwithgraph/";
	findAllStory13();
});