var rootURLTotalDuration2 = "";
	
var findAllTotalDuration2 = function() {

	$.ajax({
		type : 'GET',
		url : rootURLTotalDuration2,
		dataType : "json",
		success : renderListTotalDuration2
	});
};

var renderListTotalDuration2 = function(data) {
	
	$("#table_bodyTotalDuration2").empty();
	var list = data == null ? [] : (data instanceof Array ? data : [ data ]);
	$("#exampleTotalDuration2").show();
	$.each(list, function(index, mov) {
		var date = new Date(mov[0]).toLocaleDateString();//Date with GMT and Time
		$('#table_bodyTotalDuration2').append(
				'<tr><td>' + date + '</td><td>' + mov[1] +'</td><td>' + mov[2] +'</td><td>' 
				+ mov[3] +'</td></tr>');
	});
	$('#exampleTotalDuration2').dataTable();
};

function myFunctionTotalDuration2() {
	var modelNumber = document.getElementById("model2").value;
	rootURLTotalDuration2 += modelNumber;	
}

function findByModel(searchModel) {
	console.log('findByImsi: ' + searchImsi);
	$.ajax({
		type: 'GET',
		url: "rest/base/search/phonemodel/" + searchImsi,
		dataType: "json",
		success: renderListModel 
	});
}

var renderListModel = function(data) {
	 $( "#model" ).autocomplete({
	      source: data
	 });
};
var searchString10 = "";
$(document).ready(function(){
	$("#exampleTotalDuration2").hide();
	$("#doSearchAgainTotalDuration2").click(function(){
		rootURLTotalDuration2 = "rest/base/story10modelfailures/";
  		myFunctionTotalDuration2();
		 findAllTotalDuration2();
		 return false;
	  });
	
	$('#model').on('keydown', function(e){//NEED KEYDOWN FOR BACKSPACE
		if(e.which == 8){
			searchString10 = searchString10.slice(0, -1);
		}
	});
	$('#model').on('keypress', function(e){
		var imsiInput = $('#model').val();
			if(searchString10.length == 0){
				searchString10 = imsiInput;
			}
			else{
				var lastChar = imsiInput.slice(-1);
				searchString10 = searchString10 + lastChar;
			}	
			findByModel($('#model').val());
			console.log("search");
			console.log(searchString10);
	});
});