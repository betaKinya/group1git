var rootURLAffectedImsi = "";

var oTable3;	

$(document).ready(function(){
	$("#failureclassaffectedimsis").hide();
	$("#doSearchForAffectedImsi").click(function() {
		
		var failureDropNumeralValue = 0;
		var failuredrop = document.getElementById("failuredrop2").value;
		console.log(failuredrop);
		if(failuredrop == "DEFAULT"){
			rootURLAffectedImsi = "rest/base/datatable4";
		}
		else{
			if(failuredrop == "EMERGENCY"){
				failureDropNumeralValue = 0;
			}
			else if(failuredrop == "HIGHPRIORITYACCESS"){
				failureDropNumeralValue = 1;
			}
			else if(failuredrop == "MTACCESS"){
				failureDropNumeralValue = 2;
			}
			else if(failuredrop == "MOSIGNALLING"){
				failureDropNumeralValue = 3;
			}
			else if(failuredrop == "MODATA"){
				failureDropNumeralValue = 4;
			}
			rootURLAffectedImsi = "rest/base/datatable4/" + failureDropNumeralValue;
		}
		$("#failureclassaffectedimsis").show();
		if(typeof oTable3 == "undefined"){
			oTable3 =  $('#failureclassaffectedimsis').dataTable({
		    "aLengthMenu": [[5, 10, 20, -1], [5, 10, 20, "All"]],
		    "iDisplayLength": 5,
	        "bFilter": true,
	        "bRetrieve" : true,//Reinitialize datatable
	        "bLengthChange": false,
	        "sPaginationType": "full_numbers",
	        "aoColumns": [
	            { "mData": "more","mRender" : function (data,type, full){
	                	return new Date(full[0]).toLocaleDateString();
	            }},
	            { "mData": 1 },
	            { "mData": 2 }
	        ],
	        "bProcessing": true,
	        "bServerSide" :true,
	        "sAjaxSource": rootURLAffectedImsi
	    });
		}else{
			oTable3.fnClearTable();
			var oSettings = oTable3.fnSettings();
			var failureDropNumeralValue = 0;
			var failuredrop = document.getElementById("failuredrop2").value;
			console.log(failuredrop);
			if(failuredrop == "DEFAULT"){
				rootURLAffectedImsi = "rest/base/datatable4";
			}
			else{
				if(failuredrop == "EMERGENCY"){
					failureDropNumeralValue = 0;
				}
				else if(failuredrop == "HIGHPRIORITYACCESS"){
					failureDropNumeralValue = 1;
				}
				else if(failuredrop == "MTACCESS"){
					failureDropNumeralValue = 2;
				}
				else if(failuredrop == "MOSIGNALLING"){
					failureDropNumeralValue = 3;
				}
				else if(failuredrop == "MODATA"){
					failureDropNumeralValue = 4;
				}
				rootURLAffectedImsi = "rest/base/datatable4/" + failureDropNumeralValue;
			}
			oSettings.sAjaxSource  = rootURLAffectedImsi;
			oTable3.fnDraw();
		}
	});
});