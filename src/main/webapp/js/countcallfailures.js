var rootURLCountCallFailures = "";
	
var findAllOne = function() {
	myFunctionCountCallFailures();
	$.ajax({
		type : 'GET',
		url : rootURLCountCallFailures,
		dataType : "json",
		success : renderListOne
	});
};

var theData;
var renderListOne = function(data) {
	theData = data.length;
	console.log(theData);
	$("#table_body_count_call_failures").empty();
	$("#countparagraph").empty();
	var list = data == null ? [] : (data instanceof Array ? data : [ data ]);
	$("#example_count_call_failures").show();
	$("#countparagraph").append("The Total number of Call Failures: " + theData);
	$.each(list, function(index, mov) {
		var date = new Date(mov.dateTimeOfEvent).toLocaleDateString();//Date with GMT and Time
		$('#table_body_count_call_failures').append(
				'<tr><td>' + date + '</td><td>' + mov.countryOperatorMccMnc.country +'</td><td>' + mov.countryOperatorMccMnc.operator +'</td><td>' 
				+ mov.causeCode +'</td><td>' + mov.eventId +'</td><td>' + mov.failureClass +'</td><td>' 
				+ mov.networkElement + '</td></tr>');
	});
	$('#example_count_call_failures').dataTable({
		"aLengthMenu": [[5, 10, 20, -1], [5, 10, 20, "All"]],
        "iDisplayLength": 5
	});
};

function myFunctionCountCallFailures() {
	console.log("ok2");
	var startdate = document.getElementById("startdate").value;
	var enddate = document.getElementById("enddate").value;
	var imsi = document.getElementById("tags").value;
	console.log("startdate EQUALS " + startdate);
	console.log("enddate EQUALS " + enddate);
	console.log("IMSI EQUALS " + imsi);
	rootURLCountCallFailures += startdate + "/" + enddate + "/" + imsi;	
}

function findByImsiCountCallFailures(searchImsi) {
	console.log('findByImsi: ' + searchImsi);
	$.ajax({
		type: 'GET',
		url: "rest/base/search/" + searchImsi,
		dataType: "json",
		success: renderListImsiCountCallFailures
	});
}

var renderListImsiCountCallFailures = function(data) {
	 $( "#tags" ).autocomplete({
	      source: data
	 });
};
var searchStringCCF = "";
$(document).ready(function(){
	$("#example_count_call_failures").hide();
	$("#SearchCountCallFailures").click(function(){
		console.log("IN CountCallFailures");
		rootURLCountCallFailures = "rest/base/failures/";
		 findAllOne();
		 return false;
	  });
	$("#logoutbutton").click(function(){
		sessionStorage.setItem('usertype', "");
		window.location.href = "index.html";//Opens HTML Page
	  });
	$("#backbutton").click(function(){
		window.location.href = "commons.html";//Opens HTML Page
	  });
	$('#tags').on('keydown', function(e){//NEED KEYDOWN FOR BACKSPACE
		if(e.which == 8){
			searchStringCCF = searchStringCCF.slice(0, -1);
		}
	});
	$('#tags').on('keypress', function(e){
		var imsiInput = $('#tags').val();
			if(searchStringCCF.length == 0){
				searchStringCCF = imsiInput;
			}
			else{
				var lastChar = imsiInput.slice(-1);
				searchStringCCF = searchStringCCF + lastChar;
			}	
			findByImsiCountCallFailures($('#tags').val());
			console.log(searchStringCCF);
	});
});