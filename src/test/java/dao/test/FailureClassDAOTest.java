package dao.test;

import static org.junit.Assert.assertNotNull;
import group1.dao.FailureClassDAO;
import group1.tableEntities.FailureClass;

import javax.ejb.EJB;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(Arquillian.class)
public class FailureClassDAOTest {
	@Deployment
	public static Archive<?> createTestArchive() {
		return ShrinkWrap
				.create(JavaArchive.class, "Test.jar")
				.addClasses(FailureClass.class, FailureClassDAO.class)
				.addPackage(FailureClass.class.getPackage())
				.addPackage(FailureClassDAO.class.getPackage())
				.addAsManifestResource("META-INF/persistence.xml",
						"persistence.xml")
				.addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");
	}

	@EJB
	private FailureClassDAO failureClassDAO;

	private FailureClass failureClass;
	private static int id = 999;

	@Before
	public void persistdb() {
		failureClass = new FailureClass();
		failureClass.setFailureClass(id);
		failureClass.setDescription("Description");
		failureClassDAO.save(failureClass);
	}

	@Test
	public void testGetFailure() {
		FailureClass failure = failureClassDAO.getFailureClassId(id);
		assertNotNull(failure);
		assertNotNull(failure.getFailureClass());
		assertNotNull(failure.getDescription());
	}
}
