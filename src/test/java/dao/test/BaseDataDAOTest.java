package dao.test;

import static org.junit.Assert.assertNotNull;
import group1.dao.BaseDataDAO;
import group1.dao.CountryOperatorMccMncDAO;
import group1.dao.EventCauseDAO;
import group1.dao.FailureClassDAO;
import group1.dao.UeTableDAO;
import group1.tableEntities.BaseData;
import group1.tableEntities.CountryOperatorMccMnc;
import group1.tableEntities.CountryOperatorMccMncPK;
import group1.tableEntities.EventCause;
import group1.tableEntities.EventCausePK;
import group1.tableEntities.FailureClass;
import group1.tableEntities.UeTable;

import java.math.BigInteger;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.ejb.EJB;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(Arquillian.class)
public class BaseDataDAOTest {
	
	@Deployment
	public static Archive<?> createTestArchive() {
		return ShrinkWrap
				.create(JavaArchive.class, "Test.jar")
				.addClasses(BaseData.class, BaseDataDAO.class)
				.addPackage(BaseData.class.getPackage())
				.addPackage(BaseDataDAO.class.getPackage())
				.addAsManifestResource("META-INF/persistence.xml",
						"persistence.xml")
				.addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");
	}
	
	@EJB
	private BaseDataDAO baseDataDAO;
	@EJB
	private EventCauseDAO eventCauseDAO;
	@EJB
	private CountryOperatorMccMncDAO countryOperatorDAO;
	@EJB
	private UeTableDAO ueTableDAO;
	@EJB
	private FailureClassDAO failureClassDAO;
//	@EJB
//	private CellHierDAO cellHierDAO;

	private static boolean setUpIsDone = false;
	
	private String uniqueId ="1";

	private EventCause eventCause;//1
	private EventCausePK eventPk;//2
	private CountryOperatorMccMnc countryOperator;//3
	private CountryOperatorMccMncPK countryOperatorPk;//4	
//	private CellHier cellhier;//5
	private UeTable ueTable;//6
	private FailureClass failureClass;//7
	private BaseData baseData;//8
	BigInteger big = new BigInteger(String.valueOf(uniqueId));
	@Before
	public void xsetUp() {
		if (setUpIsDone) {
			return;
		}
		setUpIsDone = true;
		int EVENTID1 = 238;
		eventCause = new EventCause();
		eventPk = new EventCausePK();
		eventPk.setCauseCode(EVENTID1);
		eventPk.setEventId(EVENTID1);
		eventCause.setId(eventPk);
		eventCause.setDescription("RCC CONN SETUP-SUCCESS");
		//eventCauseDAO.save(eventCause);

		int marketOperatorID = 238;
		countryOperator = new CountryOperatorMccMnc();
		countryOperatorPk = new CountryOperatorMccMncPK();
		countryOperatorPk.setMarketMccId(marketOperatorID);
		countryOperatorPk.setOperatorMncId(marketOperatorID);
		countryOperator.setId(countryOperatorPk);
		countryOperator.setCountry("Ghana");
		countryOperator.setOperator("JohnT");
		countryOperatorDAO.save(countryOperator);

		int ueTableID = 238;
		ueTable = new UeTable();
		ueTable.setTacUeType(ueTableID);
		ueTable.setAccessCapability("Access");
		ueTable.setInputMode("INPUT MODE");
		ueTable.setManufacturerVendor("Ericsson");
		ueTable.setMarketingNameModel("AIT");
		ueTable.setOs("OS");		
		ueTable.setUeType("Sony");
		//ueTableDAO.save(ueTable);
		
		
//		
//		cellhier = new CellHier();
//		cellhier.setCellId(1111);
//		cellhier.setHier3_ID(big);
//		cellhier.setHier32_ID(big);
//		cellhier.setHier321_ID(big);
//		cellHierDAO.save(cellhier);
		
		int failureClassID = 999;
		failureClass = new FailureClass();
		failureClass.setFailureClass(failureClassID);
		failureClass.setDescription("Description");
		//failureClassDAO.save(failureClass);
		
		baseData = new BaseData();
		baseData.setIdBaseData(big);//1
		try {
			baseData.setDateTimeOfEvent(new SimpleDateFormat("yyy-MM-dd hh:mm:ss").parse("2011-01-01 00:00:00"));//2
		} catch (ParseException e) {
			e.printStackTrace();
		}
		baseData.setDuration(20);//3
		baseData.setCauseCode(eventCause.getId().getCauseCode());//4
		baseData.setFailureClass(failureClass.getFailureClass());//5
//		baseData.setCellHier(cellhier);//6
		baseData.setImsi(uniqueId);//7
		baseData.setCountryOperatorMccMnc(countryOperator);//8
		baseData.setNetworkElement("NODE B");//9
		baseData.setUeType(ueTable.getTacUeType());//10
		baseData.setEventId(eventCause.getId().getEventId());//11
		baseData.setCellID(1111);
		baseData.setHeir3ID(big);
		baseData.setHeir32ID(big);
		baseData.setHeir321ID(big);
		//baseDataDAO.save(baseData);
		baseDataDAO.update(baseData);
	
	}
	
	@Test
	public void aaatestGetBaseDataFailure() {
		List<BaseData> listBD = baseDataDAO.findAll();
		for(BaseData basedataloop : listBD){
			BaseData bd = baseDataDAO.getBaseDataById(String.valueOf(basedataloop.getIdBaseData()));
			assertNotNull(bd);
			assertNotNull(bd.getCauseCode());
//			assertNotNull(bd.getCellHier());
			assertNotNull(bd.getCountryOperatorMccMnc());
			assertNotNull(bd.getDateTimeOfEvent());
			assertNotNull(bd.getDuration());
			assertNotNull(bd.getEventId());
			assertNotNull(bd.getFailureClass());
			assertNotNull(bd.getIdBaseData());
			assertNotNull(bd.getImsi());
			assertNotNull(bd.getNetworkElement());
			assertNotNull(bd.getUeType());
		}
		

	
	}
	
}