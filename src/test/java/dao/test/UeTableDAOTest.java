package dao.test;

import static org.junit.Assert.assertNotNull;
import group1.dao.UeTableDAO;
import group1.tableEntities.UeTable;

import javax.ejb.EJB;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(Arquillian.class)
public class UeTableDAOTest {

	@Deployment
	public static Archive<?> createTestArchive() {
		return ShrinkWrap
				.create(JavaArchive.class, "Test.jar")
				.addClasses(UeTable.class, UeTableDAO.class)
				.addPackage(UeTable.class.getPackage())
				.addPackage(UeTableDAO.class.getPackage())
				.addAsManifestResource("META-INF/persistence.xml",
						"persistence.xml")
				.addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");

	}

	@EJB
	private UeTableDAO ueTableDAO;

	private UeTable ueTable;
	private int id = 238;
	private static boolean setUpIsDone = false;

	@Before
	public void setUp() {
		if (setUpIsDone) {
			return;
		}
		ueTable = new UeTable();
		ueTable.setTacUeType(id);
		ueTable.setAccessCapability("Access");
		ueTable.setInputMode("Input");
		ueTable.setManufacturerVendor("Ericsson");
		ueTable.setMarketingNameModel("AIT");
		ueTable.setOs("OS");
		ueTable.setInputMode("INPUT MODE");
		ueTable.setUeType("Sony");
		ueTableDAO.save(ueTable);
		setUpIsDone = true;
	}

	@Test
	public void btestUeTableFailure() {

		 UeTable ueTable = ueTableDAO.getUeTableById(id);
		 assertNotNull(ueTable);
		 assertNotNull(ueTable.getAccessCapability());
		 assertNotNull(ueTable.getInputMode());
		 assertNotNull(ueTable.getManufacturerVendor());
		 assertNotNull(ueTable.getMarketingNameModel());
		 assertNotNull(ueTable.getOs());
		 assertNotNull(ueTable.getTacUeType());
     }
}
