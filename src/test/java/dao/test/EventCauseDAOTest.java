package dao.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import group1.dao.EventCauseDAO;
import group1.tableEntities.EventCause;
import group1.tableEntities.EventCausePK;

import java.util.List;

import javax.ejb.EJB;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(Arquillian.class)
public class EventCauseDAOTest {

	@Deployment
	public static Archive<?> createTestArchive() {
		return ShrinkWrap
				.create(JavaArchive.class, "Test.jar")
				.addClasses(EventCause.class, EventCauseDAO.class)
				.addPackage(EventCause.class.getPackage())
				.addPackage(EventCauseDAO.class.getPackage())
				.addAsManifestResource("META-INF/persistence.xml",
						"persistence.xml")
				.addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");

	}

	private static boolean setUpIsDone = false;

	@EJB
	private EventCauseDAO eventCauseDAO;

	private EventCause ec1;
	private static EventCausePK eventPk1;
	private static final int EVENTID1 = 238;
	private EventCause ec2;
	private static EventCausePK eventPk2;
	private static final int EVENTID2 = 290;

	@Before
	public void setUp() {
		if (setUpIsDone) {
			return;
		}
		setUpIsDone = true;
		ec1 = new EventCause();
		eventPk1 = new EventCausePK();
		eventPk1.setCauseCode(EVENTID1);
		eventPk1.setEventId(EVENTID1);
		ec1.setId(eventPk1);
		ec1.setDescription("RRC CONN SETUP-SUCCESS");
		eventCauseDAO.addEventCauseListData(ec1);

		ec2 = new EventCause();
		eventPk2 = new EventCausePK();
		eventPk2.setCauseCode(EVENTID2);
		eventPk2.setEventId(EVENTID2);
		ec2.setId(eventPk2);
		ec2.setDescription("RRC CONN SETUP-UNSPECIFIED");
		eventCauseDAO.addEventCauseListData(ec2);
	}

	@Test
	public void atestPersistEventCauseToDB() {
		ec1 = eventCauseDAO.getEventCauseById(eventPk1);
		assertNotNull(ec1);
		assertNotNull(ec1.getId());
		assertNotNull(ec1.getDescription());
	}

	@Test
	public void btestEventCauseFindById() {
		ec1 = eventCauseDAO.getEventCauseById(eventPk1);
		assertEquals("Data fetch = data persisted", ec1.getDescription(),
				"RRC CONN SETUP-SUCCESS");
		assertEquals("Data fetch = data persisted", ec1.getId(), eventPk1);
	}

	@Test
	public void ctestEventCauseFindAll() {
		int id = 1;
		EventCausePK eventPk = new EventCausePK();
		eventPk.setCauseCode(id);
		eventPk.setEventId(id);
		EventCause eventCause = new EventCause();
		eventCause.setId(eventPk);
		eventCause.setDescription("Description");
		if(eventCauseDAO.findAll().contains(eventCause)){
		eventCauseDAO.delete(eventCause);
		}
		List<EventCause> eventCauseList = eventCauseDAO.findAll();
		assertEquals("size equals amount persisted", eventCauseList.size(), 2);
	}

//	@Test
//	public void dtestDeleteEventCauseFindAll() {
//		EventCause testEventCause1 = eventCauseDAO.getEventCauseById(eventPk1);
//		EventCause testEventCause2 = eventCauseDAO.getEventCauseById(eventPk2);
//		eventCauseDAO.delete(testEventCause1);
//		eventCauseDAO.delete(testEventCause2);
//		
//		List<EventCause> eventCauseList = new ArrayList<EventCause>();
//		eventCauseList = eventCauseDAO.findAll();
//		assertEquals("Data fetch = data persisted", eventCauseList.size(), 0);
//
//	}
}