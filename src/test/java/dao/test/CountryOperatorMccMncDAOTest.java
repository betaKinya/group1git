package dao.test;

import static org.junit.Assert.assertNotNull;
import group1.dao.CountryOperatorMccMncDAO;
import group1.tableEntities.CountryOperatorMccMnc;
import group1.tableEntities.CountryOperatorMccMncPK;

import javax.ejb.EJB;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(Arquillian.class)
public class CountryOperatorMccMncDAOTest {
	
	@Deployment
	public static Archive<?> createTestArchive() {
		return ShrinkWrap
				.create(JavaArchive.class, "Test.jar")
				.addClasses(CountryOperatorMccMnc.class, CountryOperatorMccMncDAO.class)
				.addPackage(CountryOperatorMccMnc.class.getPackage())
				.addPackage(CountryOperatorMccMncDAO.class.getPackage())
				.addAsManifestResource("META-INF/persistence.xml",
						"persistence.xml")
				.addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");
	}
	
	@EJB
	private CountryOperatorMccMncDAO countryOperatorDAO;
	
	private CountryOperatorMccMnc countryOperator;
	private CountryOperatorMccMncPK countryOperatorPK;
	
	private int marketOperatorID = 239;
	
	private static boolean setUpIsDone = false;

	@Before
	public void asetUp() {
		if (setUpIsDone) {
			return;
		}
	 countryOperator = new CountryOperatorMccMnc();
	 countryOperatorPK = new CountryOperatorMccMncPK();
	 countryOperatorPK.setMarketMccId(marketOperatorID);
	 countryOperatorPK.setOperatorMncId(marketOperatorID);
	 countryOperator.setId(countryOperatorPK);
	 countryOperator.setCountry("Ghana");
	 countryOperator.setOperator("JohnT");
	 countryOperatorDAO.addCountryOperatorMccMnc(countryOperator);
		setUpIsDone = true;
	 }

	@Test
	public void btestGetCountryOperatorFailure() {
		
		CountryOperatorMccMnc countryOperator = countryOperatorDAO.getCountryOperatorMccMncByPKID(countryOperatorPK);
//		CountryOperatorMccMnc countryOperator = new CountryOperatorMccMnc();
		assertNotNull(countryOperator);
		assertNotNull(countryOperator.getId());
		assertNotNull(countryOperator.getCountry());
		assertNotNull(countryOperator.getOperator());
	}
}
