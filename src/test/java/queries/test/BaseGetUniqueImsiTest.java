package queries.test;

import static org.junit.Assert.*;
import group1.dao.BaseDataDAO;
import group1.restful.BaseDataWS;
import group1.tableEntities.BaseData;

import java.text.ParseException;
import java.util.List;

import javax.ejb.EJB;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(Arquillian.class)
public class BaseGetUniqueImsiTest {

	@Deployment
	public static Archive<?> createTestArchive() {
		return ShrinkWrap
				.create(JavaArchive.class, "Test.jar")
				.addClasses(BaseData.class, BaseDataWS.class, BaseDataDAO.class)
				.addPackage(BaseData.class.getPackage())
				.addPackage(BaseDataWS.class.getPackage())
				.addPackage(BaseDataDAO.class.getPackage())
				.addAsManifestResource("META-INF/persistence.xml",
						"persistence.xml")
				.addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");
	}

	@EJB
	private BaseDataWS baseDataWS;

	private static boolean setUpIsDone = false;

	@Before
	public void xsetUp() {
		if (setUpIsDone) {
			return;
		}
		setUpIsDone = true;
	}

	// @Test
	// public void aTestGetUniqueImsiTest() {
	// List<BaseData> bd = baseDataWS.getUniqueImsi();
	// assertEquals(2076 ,bd.size());
	// }

	//
	// // fixed for new data
	// imsi.html story 4 correct imsi and failure class
	@Test
	public void cTestGetAllFailuresGivenImsiAndFailureClass() {
		List<BaseData> bd = baseDataWS.getBaseDatabyId("191911000001050", 3);
		assertEquals(2, bd.size());
	}

	//
	// // new
	// imsi.html story 4 wrong imsi and correct failure class
	@Test
	public void cTestGetAllFailuresGivenWrongImsiAndCorrectFailureClass() {
		List<BaseData> bd = baseDataWS.getBaseDatabyId("123456789012345", 3);
		assertEquals(0, bd.size());
	}

	//
	// // new
	// imsi.html story 4 correct imsi and wrong failure class
	@Test
	public void cTestGetAllFailuresGivenCorrectImsiAndWrongFailureClass() {
		List<BaseData> bd = baseDataWS.getBaseDatabyId("191911000001050", 9);
		assertEquals(0, bd.size());
	}

	//
	// //fixed for new data
	// countcallfailures.html story 5 correct dates and imsi
	@Test
	public void dTestGetAllFailuresGivenImsiBetweenDates()
			throws IllegalArgumentException, ParseException {
		List<BaseData> bd = baseDataWS.getCountFailuresForImsiBetweenTwoDates(
				"2013-02-22", "2013-02-26", "191911000001050");
		assertEquals(2, bd.size());
	}

	// new
	// countcallfailures.html story 5 incorrect dates and imsi
	@Test
	public void dTestGetAllFailuresGivenImsiBetweenWrongDates()
			throws IllegalArgumentException, ParseException {
		List<BaseData> bd = baseDataWS.getCountFailuresForImsiBetweenTwoDates(
				"2015-02-22", "2016-02-26", "191911000001050");
		assertEquals(0, bd.size());
	}

	// new
	// countcallfailures.html story 5 incorrect dates and incorrect imsi
	@Test
	public void dTestGetAllFailuresGivenWrongImsiBetweenWrongDates()
			throws IllegalArgumentException, ParseException {
		List<BaseData> bd = baseDataWS.getCountFailuresForImsiBetweenTwoDates(
				"2015-02-22", "2016-02-26", "123456789012345");
		assertEquals(0, bd.size());
	}

	// new
	// countcallfailures.html story 5 correct dates and incorrect imsi
	@Test
	public void dTestGetAllFailuresGivenWrongImsiBetweenDates()
			throws IllegalArgumentException, ParseException {
		List<BaseData> bd = baseDataWS.getCountFailuresForImsiBetweenTwoDates(
				"2013-02-22", "2013-02-26", "123456789012345");
		assertEquals(0, bd.size());
	}

	// uniquecausecodes.html story 6 correct data
	@Test
	public void eTestUniqueCauseCodes() throws IllegalArgumentException,
			ParseException {
		List<BaseData> bd = baseDataWS.uniqueCauseCodes("191911000001050");
		assertEquals(2, bd.size());
	}

	// uniquecausecodes.html story 6 incorrect data
	@Test
	public void eTestUniqueCauseCodesWrongIMSI()
			throws IllegalArgumentException, ParseException {
		List<BaseData> bd = baseDataWS.uniqueCauseCodes("123456789012345");
		assertEquals(0, bd.size());
	}

	// fixed for new data
	// listimsiengineercallfailures.html story 7 correct dates
	@Test
	public void bTestGetAllFailuresForImsiBetweenTwoDates()
			throws IllegalArgumentException, ParseException {
		List<BaseData> baseData = baseDataWS
				.getAllFailuresForImsiBetweenTwoDates("2013-02-22",
						"2013-02-26");
		assertEquals(10888, baseData.size());
	}

	// // new
	// listimsiengineercallfailures.html story 7 wrong dates
	@Test
	public void bTestGetAllFailuresForImsiBetweenTwoWrongDates()
			throws IllegalArgumentException, ParseException {
		List<BaseData> baseData = baseDataWS
				.getAllFailuresForImsiBetweenTwoDates("2015-02-22",
						"2016-02-26");
		// boolean inList=false;
		// for(BaseData bdata:baseData){
		//
		// if("12345".equals(bdata.getImsi()))
		// inList=true;
		// }
		// assertTrue("in list",inList);
		assertEquals(0, baseData.size());
	}

	// listimsiengineercallfailures.html story 7 correct dates
	@Test
	public void fTestGetAllFailures() throws IllegalArgumentException,
			ParseException {
		List<BaseData> bd = baseDataWS.getAllImsiFailures();
		assertEquals(26348, bd.size());
	}

	// modelcallfailures.html story8 test correct dates & correct imsi
	@Test
	public void TestGetAllFailuresGivenModelBetweenDates()
			throws IllegalArgumentException, ParseException {
		List<BaseData> bd = baseDataWS.getCountFailuresForModelBetweenTwoDates(
				"2013-01-01", "2014-01-01", 33000353);
		assertEquals(30, bd.size());
	}

	// modelcallfailures.html story8 test correct dates & wrong imsi
	@Test
	public void TestGetAllFailuresGivenWrongModelBetweenDates()
			throws IllegalArgumentException, ParseException {
		List<BaseData> bd = baseDataWS.getCountFailuresForModelBetweenTwoDates(
				"2013-01-01", "2014-01-01", 12345000);
		assertEquals(0, bd.size());
	}

	// modelcallfailures.html story8 test wrong dates & correct imsi
	@Test
	public void TestGetAllFailuresGivenModelBetweenWrongDates()
			throws IllegalArgumentException, ParseException {
		List<BaseData> bd = baseDataWS.getCountFailuresForModelBetweenTwoDates(
				"2015-01-01", "2016-01-01", 33000353);
		assertEquals(0, bd.size());
	}

	// story9imsicountduration.html story 9 correct data
	@Test
	public void story9IMSICountCorrectDates() throws IllegalArgumentException,
			ParseException {
		List<BaseData> bd = baseDataWS.getImsiCountAndDurationBetweenTwoDates(
				"2013-02-22", "2013-02-26");
		assertEquals(10888, bd.size());
	}

	// story9imsicountduration.html story 9 incorrect dates
	@Test
	public void story9IMSICountWrongDates() throws IllegalArgumentException,
			ParseException {
		List<BaseData> bd = baseDataWS.getImsiCountAndDurationBetweenTwoDates(
				"2015-02-22", "2016-02-26");
		assertEquals(0, bd.size());
	}

	// story10modelfailures.html story 10 correct data
	@Test
	public void story10modelfailuresWithCorrectModel()
			throws IllegalArgumentException, ParseException {
		List<BaseData> bd = baseDataWS.getModelFailures(33000353);
		assertEquals(3, bd.size());
	}

	// story10modelfailures.html story 10 incorrect data
	@Test
	public void story10modelfailuresWithWrongModel()
			throws IllegalArgumentException, ParseException {
		List<BaseData> bd = baseDataWS.getModelFailures(11);
		assertEquals(0, bd.size());
	}

	// story11toptencombo.html story 11 correct data
	@Test
	public void story11top10Combo() throws IllegalArgumentException,
			ParseException {
		List<BaseData> bd = baseDataWS.getTopTenCombos("2013-02-22",
				"2013-02-26");
		assertEquals(10, bd.size());
	}

	// story11toptencombo.html story 11 incorrect data
	@Test
	public void story11top10ComboWrongDates() throws IllegalArgumentException,
			ParseException {
		List<BaseData> bd = baseDataWS.getTopTenCombos("2015-02-22",
				"2016-02-26");
		assertEquals(0, bd.size());
	}

	// story12toptenimsi.html story 12 correct data
	@Test
	public void story12top10IMSI() throws IllegalArgumentException,
			ParseException {
		List<BaseData> bd = baseDataWS.getTopTenCombos("2013-02-22",
				"2013-02-26");
		assertEquals(10, bd.size());
	}

	// story12toptenimsi.html story 12 incorrect data
	@Test
	public void story12top10IMSIWrongDates() throws IllegalArgumentException,
			ParseException {
		List<BaseData> bd = baseDataWS.getTopTenCombos("2015-02-22",
				"2016-02-26");
		assertEquals(0, bd.size());
	}

	// story13top10withgraph.html story 13 
	@Test
	public void story13top10Graph() throws IllegalArgumentException,
			ParseException {
		List<BaseData> bd = baseDataWS.getTopTenWithgraph();
		assertEquals(10, bd.size());
	}

	// Story14FailureClassForAffectedImsis.html story 14 correct data
	@Test
	public void story13givenCorrectFailureClass()
			throws IllegalArgumentException, ParseException {
		List<BaseData> bd = baseDataWS
				.getCountFailuresForModelBetweenTwoDates(1);
		assertEquals(4396, bd.size());
	}

	// Story14FailureClassForAffectedImsis.html story 14 incorrect data
	@Test
	public void story13givenWrongFailureClass()
			throws IllegalArgumentException, ParseException {
		List<BaseData> bd = baseDataWS
				.getCountFailuresForModelBetweenTwoDates(100);
		assertEquals(0, bd.size());
	}

}
