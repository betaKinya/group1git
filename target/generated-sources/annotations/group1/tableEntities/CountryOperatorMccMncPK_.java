package group1.tableEntities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(CountryOperatorMccMncPK.class)
public abstract class CountryOperatorMccMncPK_ {

	public static volatile SingularAttribute<CountryOperatorMccMncPK, Integer> marketMccId;
	public static volatile SingularAttribute<CountryOperatorMccMncPK, Integer> operatorMncId;

}

