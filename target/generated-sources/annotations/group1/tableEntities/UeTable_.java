package group1.tableEntities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(UeTable.class)
public abstract class UeTable_ {

	public static volatile SingularAttribute<UeTable, String> os;
	public static volatile SingularAttribute<UeTable, String> manufacturerVendor;
	public static volatile SingularAttribute<UeTable, String> ueType;
	public static volatile SingularAttribute<UeTable, String> marketingNameModel;
	public static volatile SingularAttribute<UeTable, String> inputMode;
	public static volatile SingularAttribute<UeTable, Integer> tacUeType;
	public static volatile SingularAttribute<UeTable, String> accessCapability;

}

