package group1.tableEntities;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(CountryOperatorMccMnc.class)
public abstract class CountryOperatorMccMnc_ {

	public static volatile SingularAttribute<CountryOperatorMccMnc, CountryOperatorMccMncPK> id;
	public static volatile SingularAttribute<CountryOperatorMccMnc, String> operator;
	public static volatile ListAttribute<CountryOperatorMccMnc, BaseData> baseData;
	public static volatile SingularAttribute<CountryOperatorMccMnc, String> country;

}

