package group1.tableEntities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(EventCause.class)
public abstract class EventCause_ {

	public static volatile SingularAttribute<EventCause, EventCausePK> id;
	public static volatile SingularAttribute<EventCause, String> description;

}

