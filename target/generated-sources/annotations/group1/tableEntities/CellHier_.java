package group1.tableEntities;

import java.math.BigInteger;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(CellHier.class)
public abstract class CellHier_ {

	public static volatile SingularAttribute<CellHier, Integer> cellId;
	public static volatile SingularAttribute<CellHier, BigInteger> hier32_ID;
	public static volatile SingularAttribute<CellHier, BigInteger> hier3_ID;
	public static volatile SingularAttribute<CellHier, BigInteger> hier321_ID;
	public static volatile ListAttribute<CellHier, BaseData> baseData;

}

