package group1.tableEntities;

import java.math.BigInteger;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(BaseData.class)
public abstract class BaseData_ {

	public static volatile SingularAttribute<BaseData, Integer> causeCode;
	public static volatile SingularAttribute<BaseData, BigInteger> heir32ID;
	public static volatile SingularAttribute<BaseData, CountryOperatorMccMnc> countryOperatorMccMnc;
	public static volatile SingularAttribute<BaseData, Integer> failureClass;
	public static volatile SingularAttribute<BaseData, Integer> cellID;
	public static volatile SingularAttribute<BaseData, BigInteger> idBaseData;
	public static volatile SingularAttribute<BaseData, Date> dateTimeOfEvent;
	public static volatile SingularAttribute<BaseData, Integer> duration;
	public static volatile SingularAttribute<BaseData, String> networkElement;
	public static volatile SingularAttribute<BaseData, Integer> eventId;
	public static volatile SingularAttribute<BaseData, BigInteger> heir3ID;
	public static volatile SingularAttribute<BaseData, Integer> ueType;
	public static volatile SingularAttribute<BaseData, BigInteger> heir321ID;
	public static volatile SingularAttribute<BaseData, String> imsi;

}

