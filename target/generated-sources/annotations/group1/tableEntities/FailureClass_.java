package group1.tableEntities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(FailureClass.class)
public abstract class FailureClass_ {

	public static volatile SingularAttribute<FailureClass, String> description;
	public static volatile SingularAttribute<FailureClass, Integer> failureClass;

}

