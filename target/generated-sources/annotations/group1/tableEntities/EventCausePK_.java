package group1.tableEntities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(EventCausePK.class)
public abstract class EventCausePK_ {

	public static volatile SingularAttribute<EventCausePK, Integer> causeCode;
	public static volatile SingularAttribute<EventCausePK, Integer> eventId;

}

