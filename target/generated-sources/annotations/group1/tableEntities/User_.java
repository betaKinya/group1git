package group1.tableEntities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(User.class)
public abstract class User_ {

	public static volatile SingularAttribute<User, String> username;
	public static volatile SingularAttribute<User, String> phone;
	public static volatile SingularAttribute<User, String> email;
	public static volatile SingularAttribute<User, String> address;
	public static volatile SingularAttribute<User, Integer> userId;
	public static volatile SingularAttribute<User, String> lastname;
	public static volatile SingularAttribute<User, String> gender;
	public static volatile SingularAttribute<User, String> firstname;
	public static volatile SingularAttribute<User, String> password;

}

