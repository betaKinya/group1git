var rootURL = "";
var oTable;
var oTableAll;

function myFunctionlistimsiCF() {
	var startdate = document.getElementById("startdate").value;
	var enddate = document.getElementById("enddate").value;
	$("#examplelistimsiengineercallfailures").show();
	if(typeof oTable == "undefined"){
	console.log("IF");
    oTable = $('#examplelistimsiengineercallfailures').dataTable({
        "bFilter": true,
        "bRetrieve" : true,
        "bLengthChange": false,
        "bDestroy": true,
        "sPaginationType": "full_numbers",
        "aoColumns": [
            { "mData": "more","mRender" : function (data,type, full){
                	return new Date(full[0]).toUTCString();
            }},
            { "mData": "more","mRender" : function (data,type, full){
            	return full[1].country;
            }},
            { "mData": "more","mRender" : function (data,type, full){
            	return full[1].operator;
            }},
            { "mData": 2 },
            { "mData": 3 },
            { "mData": 4 },
            { "mData": 5 },
            { "mData": 6 }
        ],
        "bProcessing": true,
        "bServerSide" :true,
        "sAjaxSource": "rest/base/datatable2/" + startdate + "/" + enddate
    });
	}else{
		console.log("ELSE");
		console.log(startdate + " " + enddate);
		oTable.fnClearTable();
		var oSettings = oTable.fnSettings();
		 oSettings.sAjaxSource  = "rest/base/datatable2/" + startdate + "/" + enddate;
		oTable.fnDraw();
	}
}

$(document).ready(function(){
	

	$("#examplelistimsiengineercallfailures").hide();
	$("#retrieveallimsidetailsbutton").click(function() {
		console.log("FINDALLDATATABLE");
		$("#examplelistimsiengineercallfailures").show();
		if(typeof oTableAll == "undefined"){
			if(typeof oTable != "undefined"){
				oTable.fnClearTable();
				console.log("CLEAR TABLE");
			}
			oTableAll =  $('#examplelistimsiengineercallfailures').dataTable({
	        "bFilter": true,
	        "bRetrieve" : true,//Reinitialize datatable
	        "bLengthChange": false,
	        "sPaginationType": "full_numbers",
	        "aoColumns": [
	            { "mData": "more","mRender" : function (data,type, full){
	                	return new Date(full.dateTimeOfEvent).toUTCString();
	            }},
	            { "mData": "countryOperatorMccMnc.country" },
	            { "mData": "countryOperatorMccMnc.operator" },
	            { "mData": "causeCode" },
	            { "mData": "eventId" },
	            { "mData": "failureClass" },
	            { "mData": "networkElement" },
	            { "mData": "imsi"}
	        ],
	        "bProcessing": true,
	        "bServerSide" :true,
	        "sAjaxSource": "rest/base/datatable"
	    });
		}else{
			console.log("ELSE");
			console.log(startdate + " " + enddate);
			oTableAll.fnClearTable();
			var oSettings = oTableAll.fnSettings();
			oSettings.sAjaxSource  = "rest/base/datatable";
			oTableAll.fnDraw();
		}
	});
	
	$("#doSearchlistimsiengineercallfailures").click(function(){
		rootURL = "rest/base/allfailures/";
		 myFunctionlistimsiCF();
		 //findAll();
		 return false;
	  });
	$("#logoutbutton").click(function() {
		sessionStorage.setItem('usertype', "");
		window.location.href = "index.html";// Opens HTML Page
	});
	$("#backbutton").click(function() {
		window.location.href = "commons.html";// Opens HTML Page
	});
	
});