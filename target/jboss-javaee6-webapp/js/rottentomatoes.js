  var rootURL = "http://api.rottentomatoes.com/api/public/v1.0/lists/dvds/new_releases.json?page_limit=16&page=1&country=us&apikey=fznmdmme9g5w5xwwau8hv2pn";

  var findAll = function(){
  console.log('findAll');
  $.ajax({
  type:'GET',
  url:rootURL,
  dataType: "json",
  success:renderList
  });
  };

  var renderList = function (data){
  //JAX-RS Serializes an empty list as null, a collection of one as an object (NOT AN Array of One)
  var list = data == null ? [] : (data instanceof Array ? data : [data]);
  
  $.each(list,function(index, movie){
	  console.log("HELLOKU");
	  console.log(movie);
	  for(var i = 0; i < movie.movies.length; i++){
		  var movieObject = movie.movies[i];
		  var movieTitle = 	movieObject.title;
		  var castMembers = "";
		  for(var j = 0; j < movieObject.abridged_cast.length; j++){
				castMembers =  castMembers +" " + movieObject.abridged_cast[j].name;
		  }
		  var movieId	= movieObject.id;
		  var alternateLink = movieObject.links.alternate;
		  var castLink = movieObject.links.cast;
		  var clipsLink = movieObject.links.clips;
		  var reviewsLink = movieObject.links.reviews;
		  var selfLink = movieObject.links.self;
		  var similarLink = movieObject.links.similar;
			//mpaa_rating
		  var mpaaRating = movieObject.mpaa_rating;
			//ratings
          var rating = movieObject.ratings.audience_rating;
			//release_dates
		  var dvdReleaseDate = movieObject.release_dates.dvd;
		  var theaterReleaseDate = movieObject.release_dates.theater;
			//runtime
		  var runtime = movieObject.runtime;
			//synopsis
		  var synopsis = movieObject.synopsis;
			//year
		  var year = movieObject.year;
		  $('#classics_table_body').append('<tr><td>'+movieTitle+'</td><td>'+castMembers+'</td><td>'+synopsis+'</td><td>'+year+'</td></tr>');
	  }
  	//$('#table_body').append('<tr><td>'+movie.movieID+'</td><td>'+movie.rank+'</td><td>'+movie.rating +'</td><td>'+movie.title+'</td><td>'+movie.votes+'</td><td>'+movie.genre+'</td><td>'+movie.year+'</td><td>'+movie.studio+'</td><td>'+movie.availableStock+'</td><td>'+movie.status+'</td><td>'+movie.sound+'</td><td>'+movie.versions+'</td><td>'+movie.price+'</td><td>'+movie.aspect+'</td><td>'+movie.description+'</td><tr>');
  });
  };
 
  //Retrieve the wine list when the DOM is ready
  $(document).ready(function(){
  findAll();
  });




//Rotten Tomatoes Latest DVD RELEASE JSON OBJECT STRUCTURE
//var movieObject =


/*{
	"total" : 20,
	"movies" : [
			{
				"id" : "771371220",
				"title" : "24 Exposures",
				"year" : 2014,
				"mpaa_rating" : "Unrated",
				"runtime" : 80,
				"release_dates" : {
					"theater" : "2014-01-24",
					"dvd" : "2014-05-26"
				},
				"ratings" : {
					"critics_rating" : "Rotten",
					"critics_score" : 25,
					"audience_rating" : "Spilled",
					"audience_score" : 8
				},
				"synopsis" : "Fetish photographer Billy (Adam Wingard) stages elaborate shoots around his fixations, photographing women in various stages of undress and death. When one of Billy's models winds up actually dead, depressed and romantically disillusioned investigator Michael (Simon Barrett), suffering through a crumbling relationship himself, knocks on his door and discovers a world he struggles to understand - in Billy's art, as well as his apparent ease at juggling professional and intimate relationships with a committed girlfriend and various models. Jealousy runs high, and meanwhile, a killer is still on the loose. (c) IFC",
				"posters" : {
					"thumbnail" : "http://content9.flixster.com/movie/11/17/54/11175455_mob.jpg",
					"profile" : "http://content9.flixster.com/movie/11/17/54/11175455_pro.jpg",
					"detailed" : "http://content9.flixster.com/movie/11/17/54/11175455_det.jpg",
					"original" : "http://content9.flixster.com/movie/11/17/54/11175455_ori.jpg"
				},
				"abridged_cast" : [ {
					"name" : "Adam Wingard",
					"id" : "770684512",
					"characters" : [ "Billy" ]
				}, {
					"name" : "Simon Barrett",
					"id" : "771087994",
					"characters" : [ "Michael" ]
				}, {
					"name" : "Sophia Takal",
					"id" : "770907816",
					"characters" : [ "Callie" ]
				}, {
					"name" : "Helen Rogers",
					"id" : "771403667",
					"characters" : [ "Rebecca" ]
				}, {
					"name" : "Caroline White",
					"id" : "770872386",
					"characters" : [ "Alex" ]
				} ],
				"alternate_ids" : {
					"imdb" : "2082156"
				},
				"links" : {
					"self" : "http://api.rottentomatoes.com/api/public/v1.0/movies/771371220.json",
					"alternate" : "http://www.rottentomatoes.com/m/24_exposures/",
					"cast" : "http://api.rottentomatoes.com/api/public/v1.0/movies/771371220/cast.json",
					"clips" : "http://api.rottentomatoes.com/api/public/v1.0/movies/771371220/clips.json",
					"reviews" : "http://api.rottentomatoes.com/api/public/v1.0/movies/771371220/reviews.json",
					"similar" : "http://api.rottentomatoes.com/api/public/v1.0/movies/771371220/similar.json"
				}
			},
			{
				"id" : "771359562",
				"title" : "Cold Turkey",
				"year" : 2013,
				"mpaa_rating" : "Unrated",
				"runtime" : 84,
				"release_dates" : {
					"theater" : "2013-11-15",
					"dvd" : "2014-05-26"
				},
				"ratings" : {
					"critics_rating" : "Rotten",
					"critics_score" : 20,
					"audience_rating" : "Spilled",
					"audience_score" : 43
				},
				"synopsis" : "Thanksgiving get-together for the eccentric Turner clan goes from bad to worse when estranged daughter Nina (Witt) makes a surprise visit home for the first time in 15 years. Nina clashes with her stepmother Deborah (Hines), and sister, Lindsay (Walger), while half-brother Jacob (Holmes) tries to keep a massive gambling debt a secret. Meanwhile, family patriarch Poppy (Bogdanovich) has his own dramatic news to share. Cold Turkey is a black comedy about how - despite our best efforts - we all eventually turn into our parents. (c) FilmBuff",
				"posters" : {
					"thumbnail" : "http://content7.flixster.com/movie/11/17/41/11174141_mob.jpg",
					"profile" : "http://content7.flixster.com/movie/11/17/41/11174141_pro.jpg",
					"detailed" : "http://content7.flixster.com/movie/11/17/41/11174141_det.jpg",
					"original" : "http://content7.flixster.com/movie/11/17/41/11174141_ori.jpg"
				},
				"abridged_cast" : [ {
					"name" : "Peter Bogdanovich",
					"id" : "162655999",
					"characters" : [ "Poppy" ]
				}, {
					"name" : "Ashton Holmes",
					"id" : "162654545",
					"characters" : [ "Jacob" ]
				}, {
					"name" : "Alicia Witt",
					"id" : "162670451",
					"characters" : [ "Nina" ]
				}, {
					"name" : "Sonya Walger",
					"id" : "770679668",
					"characters" : [ "Lindsay" ]
				}, {
					"name" : "Ross Partridge",
					"id" : "770696484",
					"characters" : [ "TJ" ]
				} ],
				"alternate_ids" : {
					"imdb" : "2357263"
				},
				"links" : {
					"self" : "http://api.rottentomatoes.com/api/public/v1.0/movies/771359562.json",
					"alternate" : "http://www.rottentomatoes.com/m/cold_turkey_2013/",
					"cast" : "http://api.rottentomatoes.com/api/public/v1.0/movies/771359562/cast.json",
					"clips" : "http://api.rottentomatoes.com/api/public/v1.0/movies/771359562/clips.json",
					"reviews" : "http://api.rottentomatoes.com/api/public/v1.0/movies/771359562/reviews.json",
					"similar" : "http://api.rottentomatoes.com/api/public/v1.0/movies/771359562/similar.json"
				}
			},
			{
				"id" : "771376281",
				"title" : "Buttwhistle",
				"year" : 2014,
				"mpaa_rating" : "Unrated",
				"runtime" : 93,
				"release_dates" : {
					"theater" : "2014-04-25",
					"dvd" : "2014-05-26"
				},
				"ratings" : {
					"critics_score" : -1,
					"audience_rating" : "Spilled",
					"audience_score" : 0
				},
				"synopsis" : "Good catch? Ogden Confer lost a girl. Buttwhistle found a girl. A cautionary tale about Ogden Confer, a community college student who, while dealing with the recent loss of his best pal, Rose, foils the suicide effort of a mysterious young lady, Beth, who proceeds to make him pay for not minding his own business. If a girl falls, do you help her up, dust her off and send her on her way? Or do you help her up, dust her off and invite her home for dinner? (C) Breaking Glass",
				"posters" : {
					"thumbnail" : "http://content9.flixster.com/movie/11/17/69/11176999_mob.jpg",
					"profile" : "http://content9.flixster.com/movie/11/17/69/11176999_pro.jpg",
					"detailed" : "http://content9.flixster.com/movie/11/17/69/11176999_det.jpg",
					"original" : "http://content9.flixster.com/movie/11/17/69/11176999_ori.jpg"
				},
				"abridged_cast" : [ {
					"name" : "Trevor Morgan",
					"id" : "162658590"
				}, {
					"name" : "Elizabeth Rice",
					"id" : "770678223"
				}, {
					"name" : "Analeigh Tipton",
					"id" : "770876603"
				}, {
					"name" : "Adhir Kalyan",
					"id" : "770790744"
				}, {
					"name" : "Stella Maeve",
					"id" : "770689300"
				} ],
				"alternate_ids" : {
					"imdb" : "1959526"
				},
				"links" : {
					"self" : "http://api.rottentomatoes.com/api/public/v1.0/movies/771376281.json",
					"alternate" : "http://www.rottentomatoes.com/m/buttwhistle/",
					"cast" : "http://api.rottentomatoes.com/api/public/v1.0/movies/771376281/cast.json",
					"clips" : "http://api.rottentomatoes.com/api/public/v1.0/movies/771376281/clips.json",
					"reviews" : "http://api.rottentomatoes.com/api/public/v1.0/movies/771376281/reviews.json",
					"similar" : "http://api.rottentomatoes.com/api/public/v1.0/movies/771376281/similar.json"
				}
			},
			{
				"id" : "771325349",
				"title" : "Vergiss mein nicht (Forget Me Not)",
				"year" : 2013,
				"mpaa_rating" : "Unrated",
				"runtime" : 88,
				"release_dates" : {
					"dvd" : "2014-05-26"
				},
				"ratings" : {
					"critics_score" : -1,
					"audience_score" : 100
				},
				"synopsis" : "Filmmaker David Sieveking documents the time he spent caring for his mother during her battle with Alzheimer's disease, as well as the effects that her gradual mental decline had on their immediate and extended family. ~ Jason Buchanan, Rovi",
				"posters" : {
					"thumbnail" : "http://images.rottentomatoescdn.com/images/redesign/poster_default.gif",
					"profile" : "http://images.rottentomatoescdn.com/images/redesign/poster_default.gif",
					"detailed" : "http://images.rottentomatoescdn.com/images/redesign/poster_default.gif",
					"original" : "http://images.rottentomatoescdn.com/images/redesign/poster_default.gif"
				},
				"abridged_cast" : [ {
					"name" : "David Sieveking",
					"id" : "770873348"
				}, {
					"name" : "Margaret Ziveking",
					"id" : "771467200"
				}, {
					"name" : "Malta Ziveking",
					"id" : "771467201"
				}, {
					"name" : "David Ziveking",
					"id" : "771467202"
				} ],
				"alternate_ids" : {
					"imdb" : "2389530"
				},
				"links" : {
					"self" : "http://api.rottentomatoes.com/api/public/v1.0/movies/771325349.json",
					"alternate" : "http://www.rottentomatoes.com/m/forget_me_not_2013/",
					"cast" : "http://api.rottentomatoes.com/api/public/v1.0/movies/771325349/cast.json",
					"clips" : "http://api.rottentomatoes.com/api/public/v1.0/movies/771325349/clips.json",
					"reviews" : "http://api.rottentomatoes.com/api/public/v1.0/movies/771325349/reviews.json",
					"similar" : "http://api.rottentomatoes.com/api/public/v1.0/movies/771325349/similar.json"
				}
			},
			{
				"id" : "771368158",
				"title" : "Independence Daysaster",
				"year" : 2013,
				"mpaa_rating" : "Unrated",
				"runtime" : "",
				"release_dates" : {
					"dvd" : "2014-05-26"
				},
				"ratings" : {
					"critics_score" : -1,
					"audience_score" : 0
				},
				"synopsis" : "",
				"posters" : {
					"thumbnail" : "http://images.rottentomatoescdn.com/images/redesign/poster_default.gif",
					"profile" : "http://images.rottentomatoescdn.com/images/redesign/poster_default.gif",
					"detailed" : "http://images.rottentomatoescdn.com/images/redesign/poster_default.gif",
					"original" : "http://images.rottentomatoescdn.com/images/redesign/poster_default.gif"
				},
				"abridged_cast" : [ {
					"name" : "Tom Everett Scott",
					"id" : "162661411"
				}, {
					"name" : "Ryan Merriman",
					"id" : "162675132"
				}, {
					"name" : "Emily Holmes",
					"id" : "417268147"
				}, {
					"name" : "Andrea Brooks",
					"id" : "770807554"
				}, {
					"name" : "Keenan Tracey",
					"id" : "771077272"
				} ],
				"alternate_ids" : {
					"imdb" : "2645670"
				},
				"links" : {
					"self" : "http://api.rottentomatoes.com/api/public/v1.0/movies/771368158.json",
					"alternate" : "http://www.rottentomatoes.com/m/independence_daysaster/",
					"cast" : "http://api.rottentomatoes.com/api/public/v1.0/movies/771368158/cast.json",
					"clips" : "http://api.rottentomatoes.com/api/public/v1.0/movies/771368158/clips.json",
					"reviews" : "http://api.rottentomatoes.com/api/public/v1.0/movies/771368158/reviews.json",
					"similar" : "http://api.rottentomatoes.com/api/public/v1.0/movies/771368158/similar.json"
				}
			},
			{
				"id" : "771356470",
				"title" : "Endless Love",
				"year" : 2014,
				"mpaa_rating" : "PG-13",
				"runtime" : 105,
				"critics_consensus" : "Blander than the original Endless Love and even less faithful to the source material, this remake is cliched and unintentionally silly.",
				"release_dates" : {
					"theater" : "2014-02-14",
					"dvd" : "2014-05-27"
				},
				"ratings" : {
					"critics_rating" : "Rotten",
					"critics_score" : 15,
					"audience_rating" : "Upright",
					"audience_score" : 63
				},
				"synopsis" : "Alex Pettyfer (Magic Mike) and Gabriella Wilde (The Three Musketeers) star in Universal Pictures' ENDLESS LOVE, the story of a privileged girl and a charismatic boy whose instant desire sparks a love affair made only more reckless by parents trying to keep them apart. Directed by Shana Feste (Country Strong), the romantic drama co-stars Robert Patrick, Bruce Greenwood, Rhys Wakefield, Dayo Okeniyi, Emma Rigby and Joely Richardson. Scott Stuber and Pamela Abdy (Identity Thief) of Bluegrass Films are joined by Josh Schwartz and Stephanie Savage (Gossip Girl) of Fake Empire to produce the film from writers Josh Safran (Gossip Girl), Feste and Schwartz. Inspired by the 1979 novel and 1981 film of the same name, ENDLESS LOVE will be released on Valentine's Day, 2014.(c) Universal",
				"posters" : {
					"thumbnail" : "http://content7.flixster.com/movie/11/17/82/11178221_mob.jpg",
					"profile" : "http://content7.flixster.com/movie/11/17/82/11178221_pro.jpg",
					"detailed" : "http://content7.flixster.com/movie/11/17/82/11178221_det.jpg",
					"original" : "http://content7.flixster.com/movie/11/17/82/11178221_ori.jpg"
				},
				"abridged_cast" : [ {
					"name" : "Alex Pettyfer",
					"id" : "326298019",
					"characters" : [ "David Elliot" ]
				}, {
					"name" : "Gabriella Wilde",
					"id" : "771099196",
					"characters" : [ "Jade Butterfield" ]
				}, {
					"name" : "Robert Patrick",
					"id" : "162655397",
					"characters" : [ "Harry Elliot" ]
				}, {
					"name" : "Bruce Greenwood",
					"id" : "162654291",
					"characters" : [ "Hugh Butterfield" ]
				}, {
					"name" : "Rhys Wakefield",
					"id" : "770706084",
					"characters" : [ "Keith Butterfield" ]
				} ],
				"alternate_ids" : {
					"imdb" : "2318092"
				},
				"links" : {
					"self" : "http://api.rottentomatoes.com/api/public/v1.0/movies/771356470.json",
					"alternate" : "http://www.rottentomatoes.com/m/endless_love_2014/",
					"cast" : "http://api.rottentomatoes.com/api/public/v1.0/movies/771356470/cast.json",
					"clips" : "http://api.rottentomatoes.com/api/public/v1.0/movies/771356470/clips.json",
					"reviews" : "http://api.rottentomatoes.com/api/public/v1.0/movies/771356470/reviews.json",
					"similar" : "http://api.rottentomatoes.com/api/public/v1.0/movies/771356470/similar.json"
				}
			},
			{
				"id" : "771351722",
				"title" : "Cheap Thrills",
				"year" : 2014,
				"mpaa_rating" : "Unrated",
				"runtime" : 85,
				"critics_consensus" : "Gleefully nasty and darkly hilarious, Cheap Thrills lives down to its title in the best possible way.",
				"release_dates" : {
					"theater" : "2014-03-21",
					"dvd" : "2014-05-27"
				},
				"ratings" : {
					"critics_rating" : "Certified Fresh",
					"critics_score" : 85,
					"audience_rating" : "Upright",
					"audience_score" : 79
				},
				"synopsis" : "Cheap Thrills follows Craig (Pat Healy, Compliance), a struggling family man who loses his low-wage job and is threatened with eviction. In an effort to delay facing the music at home, he heads to a local bar and encounters an old friend (Ethan Embry, Empire Records). The two friends are roped into a round of drinks by a charismatic and obscenely wealthy stranger (David Koechner, Anchorman 2) along with his mysterious wife (Sara Paxton, The Inkeepers). The couple engages the two friends in a series of innocent dares in exchange for money over the course of the evening, with each challenge upping the ante in both reward and boundaries. It seems like easy and much needed money, but the couple's twisted sense of humor pushes just how far Craig and his friend are willing to go for money and cheap thrills. (c) Drafthouse",
				"posters" : {
					"thumbnail" : "http://content7.flixster.com/movie/11/17/54/11175429_mob.jpg",
					"profile" : "http://content7.flixster.com/movie/11/17/54/11175429_pro.jpg",
					"detailed" : "http://content7.flixster.com/movie/11/17/54/11175429_det.jpg",
					"original" : "http://content7.flixster.com/movie/11/17/54/11175429_ori.jpg"
				},
				"abridged_cast" : [ {
					"name" : "Pat Healy",
					"id" : "410745293",
					"characters" : [ "Craig" ]
				}, {
					"name" : "Ethan Embry",
					"id" : "162664368",
					"characters" : [ "Vince" ]
				}, {
					"name" : "Sara Paxton",
					"id" : "326298387",
					"characters" : [ "Violet" ]
				}, {
					"name" : "David Koechner",
					"id" : "162654117",
					"characters" : [ "Colin" ]
				}, {
					"name" : "Amanda Fuller",
					"id" : "770687682"
				} ],
				"alternate_ids" : {
					"imdb" : "2389182"
				},
				"links" : {
					"self" : "http://api.rottentomatoes.com/api/public/v1.0/movies/771351722.json",
					"alternate" : "http://www.rottentomatoes.com/m/cheap_thrills_2013/",
					"cast" : "http://api.rottentomatoes.com/api/public/v1.0/movies/771351722/cast.json",
					"clips" : "http://api.rottentomatoes.com/api/public/v1.0/movies/771351722/clips.json",
					"reviews" : "http://api.rottentomatoes.com/api/public/v1.0/movies/771351722/reviews.json",
					"similar" : "http://api.rottentomatoes.com/api/public/v1.0/movies/771351722/similar.json"
				}
			},
			{
				"id" : "771237501",
				"title" : "Gambit",
				"year" : 2013,
				"mpaa_rating" : "PG-13",
				"runtime" : 89,
				"release_dates" : {
					"dvd" : "2014-05-27"
				},
				"ratings" : {
					"critics_rating" : "Rotten",
					"critics_score" : 19,
					"audience_score" : 80
				},
				"synopsis" : "Colin Firth and Cameron Diaz headline this remake of the 1966 crime caper directed by Michael Hoffman (The Last Station) and written by Joel and Ethan Coen. A British thief (Firth) discovers that no plan is infallible when he recruits a beautiful woman (Diaz) to help him steal a priceless statue from an impossibly wealthy widower (Alan Rickman). Despite the fact that his pretty accomplice bears an uncanny resemblance to his affluent target's late wife, things quickly spin out of control once the job gets under way. ~ Jason Buchanan, Rovi",
				"posters" : {
					"thumbnail" : "http://content9.flixster.com/movie/11/17/82/11178291_mob.jpg",
					"profile" : "http://content9.flixster.com/movie/11/17/82/11178291_pro.jpg",
					"detailed" : "http://content9.flixster.com/movie/11/17/82/11178291_det.jpg",
					"original" : "http://content9.flixster.com/movie/11/17/82/11178291_ori.jpg"
				},
				"abridged_cast" : [ {
					"name" : "Colin Firth",
					"id" : "162654659",
					"characters" : [ "Harry Deane" ]
				}, {
					"name" : "Alan Rickman",
					"id" : "162666132",
					"characters" : [ "Lionel Shabander" ]
				}, {
					"name" : "Cameron Diaz",
					"id" : "162654443",
					"characters" : [ "PJ Puznowski" ]
				}, {
					"name" : "Tom Courtenay",
					"id" : "162674631",
					"characters" : [ "The Major" ]
				}, {
					"name" : "Stanley Tucci",
					"id" : "162661152",
					"characters" : [ "Zaidenweber" ]
				} ],
				"alternate_ids" : {
					"imdb" : "0404978"
				},
				"links" : {
					"self" : "http://api.rottentomatoes.com/api/public/v1.0/movies/771237501.json",
					"alternate" : "http://www.rottentomatoes.com/m/gambit_2012/",
					"cast" : "http://api.rottentomatoes.com/api/public/v1.0/movies/771237501/cast.json",
					"clips" : "http://api.rottentomatoes.com/api/public/v1.0/movies/771237501/clips.json",
					"reviews" : "http://api.rottentomatoes.com/api/public/v1.0/movies/771237501/reviews.json",
					"similar" : "http://api.rottentomatoes.com/api/public/v1.0/movies/771237501/similar.json"
				}
			},
			{
				"id" : "771354623",
				"title" : "Run & Jump",
				"year" : 2013,
				"mpaa_rating" : "Unrated",
				"runtime" : 105,
				"release_dates" : {
					"theater" : "2014-01-24",
					"dvd" : "2014-05-27"
				},
				"ratings" : {
					"critics_rating" : "Fresh",
					"critics_score" : 86,
					"audience_rating" : "Upright",
					"audience_score" : 60
				},
				"synopsis" : "A headstrong Irish housewife finds her life transforming in ways she never thought possible after her husband suffers a life-altering stroke, and an American doctor arrives to chronicle the family's recovery process in this intimate drama from director Steph Green (whose short film New Boy was nominated for an Oscar in 2007). In the wake of her husband's stroke, loving wife and mother Vanetia (Maxine Peake) gradually comes to realize that her household will never be the same again. Much to Vanetia's relief, a research grant from American doctor Ted Fielding (Will Forte) provides the funds needed to remain financially afloat. Ted wants to study how the family copes with such a severe trauma, and though at first his presence in the home strikes a chord of resentment in the overburdened Vanetia, he exhibits an air of tranquility that soon becomes a source of deep comfort to her. Likewise, Vanetia's unwavering strong will awakens a newfound sense of vitality in the reserved Dr. Fielding, resulting in growth and healing for all involved. ~ Jason Buchanan, Rovi",
				"posters" : {
					"thumbnail" : "http://content9.flixster.com/movie/11/17/55/11175591_mob.jpg",
					"profile" : "http://content9.flixster.com/movie/11/17/55/11175591_pro.jpg",
					"detailed" : "http://content9.flixster.com/movie/11/17/55/11175591_det.jpg",
					"original" : "http://content9.flixster.com/movie/11/17/55/11175591_ori.jpg"
				},
				"abridged_cast" : [ {
					"name" : "Maxine Peake",
					"id" : "770699240",
					"characters" : [ "Vanetia Casey" ]
				}, {
					"name" : "Will Forte",
					"id" : "770670480",
					"characters" : [ "Dr. Ted Fielding" ]
				}, {
					"name" : "Edward Macliam",
					"id" : "771057090",
					"characters" : [ "Conor Casey" ]
				}, {
					"name" : "Brendan Morris",
					"id" : "771443436",
					"characters" : [ "Lenny" ]
				}, {
					"name" : "Ciara Gallagher",
					"id" : "771446346",
					"characters" : [ "Noni" ]
				} ],
				"alternate_ids" : {
					"imdb" : "2343158"
				},
				"links" : {
					"self" : "http://api.rottentomatoes.com/api/public/v1.0/movies/771354623.json",
					"alternate" : "http://www.rottentomatoes.com/m/run_and_jump_2013/",
					"cast" : "http://api.rottentomatoes.com/api/public/v1.0/movies/771354623/cast.json",
					"clips" : "http://api.rottentomatoes.com/api/public/v1.0/movies/771354623/clips.json",
					"reviews" : "http://api.rottentomatoes.com/api/public/v1.0/movies/771354623/reviews.json",
					"similar" : "http://api.rottentomatoes.com/api/public/v1.0/movies/771354623/similar.json"
				}
			},
			{
				"id" : "771354533",
				"title" : "A Birder's Guide To Everything",
				"year" : 2013,
				"mpaa_rating" : "PG-13",
				"runtime" : 87,
				"release_dates" : {
					"theater" : "2014-03-21",
					"dvd" : "2014-05-27"
				},
				"ratings" : {
					"critics_rating" : "Fresh",
					"critics_score" : 90,
					"audience_rating" : "Upright",
					"audience_score" : 73
				},
				"synopsis" : "David Portnoy (Kodi Smit-McPhee), a 15-year-old birding fanatic, thinks that he's made the discovery of a lifetime. So, on the eve of his father's remarriage, he escapes on an epic road trip with his best friends to solidify their place in birding history. (c) Official Site",
				"posters" : {
					"thumbnail" : "http://content6.flixster.com/movie/11/17/62/11176208_mob.jpg",
					"profile" : "http://content6.flixster.com/movie/11/17/62/11176208_pro.jpg",
					"detailed" : "http://content6.flixster.com/movie/11/17/62/11176208_det.jpg",
					"original" : "http://content6.flixster.com/movie/11/17/62/11176208_ori.jpg"
				},
				"abridged_cast" : [ {
					"name" : "Ben Kingsley",
					"id" : "162653703",
					"characters" : [ "Lawrence Konrad" ]
				}, {
					"name" : "James LeGros",
					"id" : "770669462",
					"characters" : [ "Donald Portnoy" ]
				}, {
					"name" : "Kodi Smit-McPhee",
					"id" : "770673434",
					"characters" : [ "David Portnoy" ]
				}, {
					"name" : "Alex Wolff",
					"id" : "459518389",
					"characters" : [ "Timmy Barsky" ]
				}, {
					"name" : "Katie Chang",
					"id" : "771399391",
					"characters" : [ "Ellen Reeves" ]
				} ],
				"alternate_ids" : {
					"imdb" : "1582465"
				},
				"links" : {
					"self" : "http://api.rottentomatoes.com/api/public/v1.0/movies/771354533.json",
					"alternate" : "http://www.rottentomatoes.com/m/a_birders_guide_to_everything_2013/",
					"cast" : "http://api.rottentomatoes.com/api/public/v1.0/movies/771354533/cast.json",
					"clips" : "http://api.rottentomatoes.com/api/public/v1.0/movies/771354533/clips.json",
					"reviews" : "http://api.rottentomatoes.com/api/public/v1.0/movies/771354533/reviews.json",
					"similar" : "http://api.rottentomatoes.com/api/public/v1.0/movies/771354533/similar.json"
				}
			},
			{
				"id" : "770675824",
				"title" : "Could This Be Love? (Je crois que je l'aime)",
				"year" : 2014,
				"mpaa_rating" : "Unrated",
				"runtime" : 88,
				"release_dates" : {
					"theater" : "2007-02-08",
					"dvd" : "2014-05-27"
				},
				"ratings" : {
					"critics_score" : -1,
					"audience_rating" : "Spilled",
					"audience_score" : 33
				},
				"synopsis" : "Sandra (Drew Sidora), a small town country girl, returns home after college to take over the family business, her father's beloved bed and breakfast. Facing an unfaithful husband and dysfunctional relationship with her mother, she finds herself at a difficult crossroads. Everything changes, however, when Terrance (Steven Sutton), a young, handsome lawyer checks in and opens her eyes to an entirely different life filled with happiness.",
				"posters" : {
					"thumbnail" : "http://content9.flixster.com/movie/10/67/63/10676383_mob.jpg",
					"profile" : "http://content9.flixster.com/movie/10/67/63/10676383_pro.jpg",
					"detailed" : "http://content9.flixster.com/movie/10/67/63/10676383_det.jpg",
					"original" : "http://content9.flixster.com/movie/10/67/63/10676383_ori.jpg"
				},
				"abridged_cast" : [ {
					"name" : "Drew Sidora",
					"id" : "326396945",
					"characters" : [ "Sandra" ]
				}, {
					"name" : "Thomas Mikal Ford",
					"id" : "770723718"
				}, {
					"name" : "Steven Sutton",
					"id" : "771492761",
					"characters" : [ "Terrance" ]
				}, {
					"name" : "Dominique DuVernay",
					"id" : "770819986"
				}, {
					"name" : "Percy Romeo Miller",
					"id" : "770784983"
				} ],
				"alternate_ids" : {
					"imdb" : "0488565"
				},
				"links" : {
					"self" : "http://api.rottentomatoes.com/api/public/v1.0/movies/770675824.json",
					"alternate" : "http://www.rottentomatoes.com/m/could-this-be-love-je-crois-que-je-laime/",
					"cast" : "http://api.rottentomatoes.com/api/public/v1.0/movies/770675824/cast.json",
					"clips" : "http://api.rottentomatoes.com/api/public/v1.0/movies/770675824/clips.json",
					"reviews" : "http://api.rottentomatoes.com/api/public/v1.0/movies/770675824/reviews.json",
					"similar" : "http://api.rottentomatoes.com/api/public/v1.0/movies/770675824/similar.json"
				}
			},
			{
				"id" : "770783627",
				"title" : "L'Eden et apres (Eden and After)",
				"year" : 1970,
				"mpaa_rating" : "Unrated",
				"runtime" : 88,
				"release_dates" : {
					"theater" : "1970-06-01",
					"dvd" : "2014-05-27"
				},
				"ratings" : {
					"critics_score" : -1,
					"audience_rating" : "Upright",
					"audience_score" : 81
				},
				"synopsis" : "",
				"posters" : {
					"thumbnail" : "http://content9.flixster.com/movie/10/85/36/10853611_mob.jpg",
					"profile" : "http://content9.flixster.com/movie/10/85/36/10853611_pro.jpg",
					"detailed" : "http://content9.flixster.com/movie/10/85/36/10853611_det.jpg",
					"original" : "http://content9.flixster.com/movie/10/85/36/10853611_ori.jpg"
				},
				"abridged_cast" : [ {
					"name" : "Catherine Jourdan",
					"id" : "770785129",
					"characters" : [ "Violette" ]
				}, {
					"name" : "Pierre Zimmer",
					"id" : "770694507",
					"characters" : [ "Duchemin" ]
				}, {
					"name" : "Richard Leduc",
					"id" : "770804864",
					"characters" : [ "Boy" ]
				}, {
					"name" : "Lorraine Rainer",
					"id" : "770717236"
				}, {
					"name" : "Ludovit Kroner",
					"id" : "770804865"
				} ],
				"alternate_ids" : {
					"imdb" : "0067040"
				},
				"links" : {
					"self" : "http://api.rottentomatoes.com/api/public/v1.0/movies/770783627.json",
					"alternate" : "http://www.rottentomatoes.com/m/leden-et-apres-eden-and-after/",
					"cast" : "http://api.rottentomatoes.com/api/public/v1.0/movies/770783627/cast.json",
					"clips" : "http://api.rottentomatoes.com/api/public/v1.0/movies/770783627/clips.json",
					"reviews" : "http://api.rottentomatoes.com/api/public/v1.0/movies/770783627/reviews.json",
					"similar" : "http://api.rottentomatoes.com/api/public/v1.0/movies/770783627/similar.json"
				}
			},
			{
				"id" : "771363960",
				"title" : "House In The Alley",
				"year" : 2013,
				"mpaa_rating" : "Unrated",
				"runtime" : 93,
				"release_dates" : {
					"theater" : "2013-10-25",
					"dvd" : "2014-05-27"
				},
				"ratings" : {
					"critics_rating" : "Rotten",
					"critics_score" : 29,
					"audience_rating" : "Upright",
					"audience_score" : 75
				},
				"synopsis" : "Until they lose their baby to a miscarriage, a young couple were happily settling into their new life in their spacious home. After the tragedy, Thao is inconsolable and won't let her baby's body leave the house. Normal life eludes her as terrifying visions undermine her sanity. Her husband, Thanh, experiences strange phenomena around their home and when his wife turns on him, he must race to uncover the secrets of the house in the alley before they lose their sanity and their lives.",
				"posters" : {
					"thumbnail" : "http://content7.flixster.com/movie/11/17/42/11174225_mob.jpg",
					"profile" : "http://content7.flixster.com/movie/11/17/42/11174225_pro.jpg",
					"detailed" : "http://content7.flixster.com/movie/11/17/42/11174225_det.jpg",
					"original" : "http://content7.flixster.com/movie/11/17/42/11174225_ori.jpg"
				},
				"abridged_cast" : [ {
					"name" : "Thanh Van Ngo",
					"id" : "770684934"
				}, {
					"name" : "Bao Son Tran",
					"id" : "771086502"
				}, {
					"name" : "Boch Hang",
					"id" : "771492937"
				}, {
					"name" : "Bui Xjuan Hai",
					"id" : "771492938"
				}, {
					"name" : "Phan Thi Mo",
					"id" : "771492939"
				} ],
				"alternate_ids" : {
					"imdb" : "2461462"
				},
				"links" : {
					"self" : "http://api.rottentomatoes.com/api/public/v1.0/movies/771363960.json",
					"alternate" : "http://www.rottentomatoes.com/m/house_in_the_alley/",
					"cast" : "http://api.rottentomatoes.com/api/public/v1.0/movies/771363960/cast.json",
					"clips" : "http://api.rottentomatoes.com/api/public/v1.0/movies/771363960/clips.json",
					"reviews" : "http://api.rottentomatoes.com/api/public/v1.0/movies/771363960/reviews.json",
					"similar" : "http://api.rottentomatoes.com/api/public/v1.0/movies/771363960/similar.json"
				}
			},
			{
				"id" : "770783626",
				"title" : "L'Homme qui ment (The Man Who Lies)",
				"year" : 1968,
				"mpaa_rating" : "Unrated",
				"runtime" : 95,
				"release_dates" : {
					"theater" : "1968-01-01",
					"dvd" : "2014-05-27"
				},
				"ratings" : {
					"critics_score" : -1,
					"audience_rating" : "Upright",
					"audience_score" : 79
				},
				"synopsis" : "",
				"posters" : {
					"thumbnail" : "http://content8.flixster.com/movie/10/85/36/10853610_mob.jpg",
					"profile" : "http://content8.flixster.com/movie/10/85/36/10853610_pro.jpg",
					"detailed" : "http://content8.flixster.com/movie/10/85/36/10853610_det.jpg",
					"original" : "http://content8.flixster.com/movie/10/85/36/10853610_ori.jpg"
				},
				"abridged_cast" : [ {
					"name" : "Jean-Louis Trintignant",
					"id" : "162667842",
					"characters" : [ "Boris" ]
				}, {
					"name" : "Ivan Mistrik",
					"id" : "770804863",
					"characters" : [ "Jean" ]
				}, {
					"name" : "Sylvie Breal",
					"id" : "770779212"
				}, {
					"name" : "Sylvia Turbova",
					"id" : "770975926",
					"characters" : [ "Sylvia" ]
				}, {
					"name" : "Josef Kroner",
					"id" : "770913744",
					"characters" : [ "Frantz" ]
				} ],
				"alternate_ids" : {
					"imdb" : "0063080"
				},
				"links" : {
					"self" : "http://api.rottentomatoes.com/api/public/v1.0/movies/770783626.json",
					"alternate" : "http://www.rottentomatoes.com/m/lhomme-qui-ment-the-man-who-lies/",
					"cast" : "http://api.rottentomatoes.com/api/public/v1.0/movies/770783626/cast.json",
					"clips" : "http://api.rottentomatoes.com/api/public/v1.0/movies/770783626/clips.json",
					"reviews" : "http://api.rottentomatoes.com/api/public/v1.0/movies/770783626/reviews.json",
					"similar" : "http://api.rottentomatoes.com/api/public/v1.0/movies/770783626/similar.json"
				}
			},
			{
				"id" : "771376208",
				"title" : "Tapped Out",
				"year" : 2014,
				"mpaa_rating" : "R",
				"runtime" : 108,
				"release_dates" : {
					"theater" : "2014-05-27",
					"dvd" : "2014-05-27"
				},
				"ratings" : {
					"critics_score" : -1,
					"audience_score" : 67
				},
				"synopsis" : "An ex-con and former martial-arts wunderkind is sentenced to community service at a decrepit karate school, where he crosses paths with the very man that killed his family ten years before. Now, he must disobey the school's sensei and train to confront the killer, this time, in the MMA octagon. ~ Cammila Collar, Rovi",
				"posters" : {
					"thumbnail" : "http://content6.flixster.com/movie/11/17/75/11177552_mob.jpg",
					"profile" : "http://content6.flixster.com/movie/11/17/75/11177552_pro.jpg",
					"detailed" : "http://content6.flixster.com/movie/11/17/75/11177552_det.jpg",
					"original" : "http://content6.flixster.com/movie/11/17/75/11177552_ori.jpg"
				},
				"abridged_cast" : [ {
					"name" : "Cody Hackman",
					"id" : "771492433"
				}, {
					"name" : "Krzysztof Soszynski",
					"id" : "770862114"
				}, {
					"name" : "Anderson \"The Spider\" Silva",
					"id" : "770903574"
				}, {
					"name" : "Lyoto Machida",
					"id" : "770774873"
				}, {
					"name" : "Jessica Brown",
					"id" : "771253414"
				} ],
				"alternate_ids" : {
					"imdb" : "1531426"
				},
				"links" : {
					"self" : "http://api.rottentomatoes.com/api/public/v1.0/movies/771376208.json",
					"alternate" : "http://www.rottentomatoes.com/m/tapped_out_2014/",
					"cast" : "http://api.rottentomatoes.com/api/public/v1.0/movies/771376208/cast.json",
					"clips" : "http://api.rottentomatoes.com/api/public/v1.0/movies/771376208/clips.json",
					"reviews" : "http://api.rottentomatoes.com/api/public/v1.0/movies/771376208/reviews.json",
					"similar" : "http://api.rottentomatoes.com/api/public/v1.0/movies/771376208/similar.json"
				}
			},
			{
				"id" : "771375734",
				"title" : "Airplane vs Volcano",
				"year" : 2014,
				"mpaa_rating" : "PG-13",
				"runtime" : 90,
				"release_dates" : {
					"dvd" : "2014-05-27"
				},
				"ratings" : {
					"critics_score" : -1,
					"audience_score" : 0
				},
				"synopsis" : "Based on a true story, Airplane vs. Volcano takes place high in the skies. Here, an airline crew and several passengers find themselves trapped in an active volcano region. There is no escape, except to fly the plane down and no one knows if anyone will survive. Airplane vs. Volcano is a harrowing story of terror, at 30,000 feet.",
				"posters" : {
					"thumbnail" : "http://images.rottentomatoescdn.com/images/redesign/poster_default.gif",
					"profile" : "http://images.rottentomatoescdn.com/images/redesign/poster_default.gif",
					"detailed" : "http://images.rottentomatoescdn.com/images/redesign/poster_default.gif",
					"original" : "http://images.rottentomatoescdn.com/images/redesign/poster_default.gif"
				},
				"abridged_cast" : [ {
					"name" : "Dean Cain",
					"id" : "162696548"
				}, {
					"name" : "Robin Givens",
					"id" : "162671458"
				}, {
					"name" : "Morgan West",
					"id" : "771492046"
				}, {
					"name" : "Tamara Goodwin",
					"id" : "771436907"
				}, {
					"name" : "Matt Mercer",
					"id" : "770769536"
				} ],
				"alternate_ids" : {
					"imdb" : "3417334"
				},
				"links" : {
					"self" : "http://api.rottentomatoes.com/api/public/v1.0/movies/771375734.json",
					"alternate" : "http://www.rottentomatoes.com/m/airplane_vs_volcano/",
					"cast" : "http://api.rottentomatoes.com/api/public/v1.0/movies/771375734/cast.json",
					"clips" : "http://api.rottentomatoes.com/api/public/v1.0/movies/771375734/clips.json",
					"reviews" : "http://api.rottentomatoes.com/api/public/v1.0/movies/771375734/reviews.json",
					"similar" : "http://api.rottentomatoes.com/api/public/v1.0/movies/771375734/similar.json"
				}
			} ],
	"links" : {
		"self" : "http://api.rottentomatoes.com/api/public/v1.0/lists/dvds/upcoming.json?page_limit=16&country=us&page=1",
		"next" : "http://api.rottentomatoes.com/api/public/v1.0/lists/dvds/upcoming.json?page_limit=16&country=us&page=2",
		"alternate" : "http://www.rottentomatoes.com/dvd/upcoming.json"
	},
	"link_template" : "http://api.rottentomatoes.com/api/public/v1.0/lists/dvds/upcoming.json?page_limit={results-per-page}&page={page-number}&country={country-code}"
};
*/
