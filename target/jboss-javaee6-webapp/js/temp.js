var rootURL = "http://localhost:8080/jboss-javaee6-webapp/rest/base/";
	
var findAll = function() {
	
	// append imsi
	myFunction();
	
	console.log('findAll');
	$.ajax({
		type : 'GET',
		url : rootURL,
		dataType : "json",
		success : renderList
	});
	
};

var renderList = function(data) {

	// JAX-RS Serializes an empty list as null, a collection of one as an object
	// (NOT AN Array of One)
	var list = data == null ? [] : (data instanceof Array ? data : [ data ]);

	$.each(list, function(index, mov) {
		console.log(mov);
		console.log("JavaScript");

		$('#table_body').append(
				'<tr><td>' + mov.id.causeCode + '</td><td>' + mov.id.eventId
						+ '</td></tr>');
		
	});
	$('#example').dataTable();
};

function myFunction() {
	console.log("ok2");
	var imsi = document.getElementById("imsiform").value;
	rootURL += imsi;	
}

$(document).ready(function(){
	$("#doSearch").click(function(){
		 findAll();
		 return false;
	  });
	console.log("ok");
});