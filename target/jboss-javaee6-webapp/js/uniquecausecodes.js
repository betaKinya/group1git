var rootURL = "";
	
var findAll = function() {
	// append imsi
	myFunction();
	console.log('findAll');
	$.ajax({
		type : 'GET',
		url : rootURL,
		dataType : "json",
		success : renderListUnique
	});
	
};

var count = 0;
var renderListUnique = function(data) {
	var list = data == null ? [] : (data instanceof Array ? data : [ data ]);
	$("#table_body_unique").empty();
	$("#exampleuniquecausecodes").show();
	console.log(data);
	$.each(list, function(index, mov) {
		console.log(mov);
		$('#table_body_unique').append(
				'<tr><td>' + mov + '</td></tr>');
	});
	$('#exampleuniquecausecodes').dataTable();
};

function myFunction() {
	console.log("ok2");
	var imsi = $('#uniquecausecodevalue').val();
	rootURL = "rest/base/uniquecausecodes/" + imsi;	
}

function findByImsi(searchImsi) {
	console.log('findByImsi: ' + searchImsi);
	$.ajax({
		type: 'GET',
		url: "rest/base/search/" + searchImsi,
		dataType: "json",
		success: renderListImsi 
	});
}

var renderListImsi = function(data) {
	 $( ".tags" ).autocomplete({
	      source: data
	 });
};

var searchString = "";
$(document).ready(function(){
	$("#exampleuniquecausecodes").hide();
	$("#doSearchuniquecausecodes").click(function(){
		console.log("HERE FINDALL UNIQUE");
		 findAll();
		 return false;
	  });
	$("#logoutbutton").click(function(){
		sessionStorage.setItem('usertype', "");
		window.location.href = "index.html";//Opens HTML Page
	  });
	$("#backbutton").click(function(){
		window.location.href = "commons.html";//Opens HTML Page
	  });
	
	$('.tags').on('keydown', function(e){//NEED KEYDOWN FOR BACKSPACE
		if(e.which == 8){
			searchString = searchString.slice(0, -1);
		}
	});
	$('.tags').on('keypress', function(e){
		var imsiInput = $('.tags').val();
			if(searchString.length == 0){
				searchString = imsiInput;
			}
			else{
				var lastChar = imsiInput.slice(-1);
				searchString = searchString + lastChar;
			}	
			findByImsi($('.tags').val());
			console.log(searchString);
	});
});