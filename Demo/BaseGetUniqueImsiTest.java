package queries.test;
	
import static org.junit.Assert.assertEquals;
import group1.dao.BaseDataDAO;
import group1.restful.BaseDataWS;
import group1.tableEntities.BaseData;

import java.math.BigInteger;
import java.text.ParseException;
import java.util.List;

import javax.ejb.EJB;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

	@FixMethodOrder(MethodSorters.NAME_ASCENDING)
	@RunWith(Arquillian.class)
	public class BaseGetUniqueImsiTest {
		
		@Deployment
		public static Archive<?> createTestArchive() {
			return ShrinkWrap
					.create(JavaArchive.class, "Test.jar")
					.addClasses(BaseData.class, BaseDataWS.class, BaseDataDAO.class)
					.addPackage(BaseData.class.getPackage())
					.addPackage(BaseDataWS.class.getPackage())
					.addPackage(BaseDataDAO.class.getPackage())
					.addAsManifestResource("META-INF/persistence.xml",
							"persistence.xml")
					.addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");
		}
		
		@EJB
		private BaseDataWS baseDataWS;

		private static boolean setUpIsDone = false;
		
	
		@Before
		public void xsetUp() {
			if (setUpIsDone) {
				return;
			}
			setUpIsDone = true;
		}
		
		@Test
		public void aTestGetUniqueImsiTest() {
			List<BaseData> bd = baseDataWS.getUniqueImsi();
			assertEquals(2076 ,bd.size());
		}
		
		@Test
		public void bTestGetAllFailuresForImsiBetweenTwoDates() throws IllegalArgumentException, ParseException {
		List<BaseData> baseData = baseDataWS.getAllFailuresForImsiBetweenTwoDates("2013-01-01", "2014-01-01");
		assertEquals(9547, baseData.size());
		}
		
		@Test
		public void cTestGetAllFailuresGivenImsiAndFailureClass() {
			List<BaseData> bd = baseDataWS.getBaseDatabyId(new BigInteger("191911000403087"), 4);
			assertEquals(4 ,bd.size());
		}	
		
		@Test
		public void dTestGetAllFailuresGivenImsiBetweenDates() throws IllegalArgumentException, ParseException {
			List<BaseData> bd = baseDataWS.getCountFailuresForImsiBetweenTwoDates("2013-01-01", "2014-01-01", new BigInteger("191911000403087"));
			assertEquals(4 ,bd.size());
		}
		
		@Test
		public void eTestUniqueCauseCodes() throws IllegalArgumentException, ParseException {
			List<BaseData> bd = baseDataWS.uniqueCauseCodes(new BigInteger("191911000403087"));
			assertEquals(1 ,bd.size());
		}
		
		@Test
		public void fTestGetAllFailures() throws IllegalArgumentException, ParseException {
			List<BaseData> bd = baseDataWS.getAllImsiFailures();
			assertEquals(2076 ,bd.size());
		}
}
